using Domain;

namespace ClientApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebApplication
                .CreateBuilder(args)
                .ConfigureServices("ClientApi")
                .Build()
                .Configure()
                .Run();
        }
    }
}
