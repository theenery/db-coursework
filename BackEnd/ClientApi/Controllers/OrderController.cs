﻿using AutoMapper;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Dto;
using Infrastructure.Extensions;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClientApi.Controllers
{
    [Authorize(Roles = "Client")]
    [Produces("application/json")]
    [Route("client-api/clients/{client}/[controller]")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderService _orderService;

        public OrderController(IOrderRepository orderRepository, IOrderService orderService, IMapper mapper) : base(mapper)
        {
            _orderRepository = orderRepository;
            _orderService = orderService;
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult<int>> GetCount(int client)
        {
            var result = await _orderRepository.GetAll(o => o.ClientId == client).CountAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientOrderDto>>> GetPaged(int client, [FromQuery] int count, [FromQuery] int page, [FromQuery] string orderBy, [FromQuery] bool isDescending)
        {
            var orders = await _orderService.GetPagedAsync(o => o.ClientId == client, count, page, orderBy, isDescending);

            return Ok(Mapper.Map<IEnumerable<ClientOrderDto>>(orders));
        }

        [HttpGet("{id}", Name = "GetOrder")]
        public async Task<ActionResult<ClientOrderDetailedDto>> GetOrder(int client, int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null || order.ClientId != client)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ClientOrderDetailedDto>(order));
        }

        [HttpPost]
        public async Task<ActionResult<ClientOrderDto>> PostOrder(int client, [FromBody] CreateOrderDto order)
        {
            var orderEntity = Mapper.Map<Order>(order);
            orderEntity.ClientId = client;
            await _orderRepository.CreateAsync(orderEntity);

            return CreatedAtRoute("GetOrder", new { client = client, id = orderEntity.OrderId }, Mapper.Map<ClientOrderDto>(orderEntity));
        }
    }
}