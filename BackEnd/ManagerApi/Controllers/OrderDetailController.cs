﻿using AutoMapper;
using Infrastructure.Dto;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Extensions;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Repository.Implementations;
namespace EmployeeApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("manager-api/[controller]")]
    [ApiController]
    public class OrderDetailController : BaseController
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailService _orderDetailService;
        private readonly IOrderDetailRepository _orderDetailRepository;

        public OrderDetailController(IOrderRepository orderRepository, IOrderDetailService orderDetailService, IOrderDetailRepository orderDetailRepository, IMapper mapper) : base(mapper)
        {
            _orderRepository = orderRepository;
            _orderDetailService = orderDetailService;
            _orderDetailRepository = orderDetailRepository;
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult<int>> GetCount([FromQuery] int orderDetailStatus, [FromQuery] string? name)
        {
            var result = await _orderDetailRepository
                .GetAll(od =>
                    od.OrderDetailStatusId == orderDetailStatus
                    && od.Detail.DetailName.Contains(name ?? ""))
                .CountAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderDetailDto>>> GetPaged([FromQuery] int orderDetailStatus, [FromQuery] string? name, [FromQuery] int count, [FromQuery] int page, [FromQuery] string orderBy, [FromQuery] bool isDescending)
        {
            var result = await _orderDetailService
                .GetPagedAsync(
                    d => d.OrderDetailStatusId == orderDetailStatus && d.Detail.DetailName.Contains(name ?? ""),
                    count,
                    page,
                    orderBy,
                    isDescending);
            return Ok(Mapper.Map<IEnumerable<OrderDetailDto>>(result));
        }

        //[HttpGet("{id}", Name = "GetDetail")]
        //public async Task<ActionResult<DetailWithDeviceTypesDto>> GetDetail(int id)
        //{
        //    var detail = await _detailRepository.GetByIdAsync(id);

        //    if (detail == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(Mapper.Map<DetailWithDeviceTypesDto>(detail));
        //}

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetail(int id, OrderDetailDto orderDetail)
        {
            if (id != orderDetail.DetailId)
            {
                return BadRequest();
            }

            var od = Mapper.Map<OrderDetail>(orderDetail);
            od.OrderDetailStatusId = od.OrderDetailStatus.OrderDetailStatusId;
            od.OrderDetailStatus = null;

            var o = await _orderRepository.GetByIdAsync(od.OrderId);

            if (o.OrderStatusId != 4)
            {
                if (o.OrderDetails.Any(od => od.OrderDetailStatusId <= 2))
                {
                    o.OrderStatusId = 3;
                    o.OrderStatus = null;
                }
                else
                {
                    o.OrderStatusId = 2;
                    o.OrderStatus = null;
                }
            }

            await _orderDetailRepository.UpdateAsync(id, orderDetail.OrderId, od);

            return NoContent();
        }

        //[HttpPost]
        //public async Task<ActionResult<DetailWithDeviceTypesDto>> PostDetail(DetailWithDeviceTypesDto detail)
        //{
        //    var detailEntity = Mapper.Map<Detail>(detail);
        //    var deviceTypes = detailEntity.DeviceTypes;
        //    detailEntity.DeviceTypes = new List<DeviceType>();
        //    foreach (var deviceType in deviceTypes) {
        //        detailEntity.DeviceTypes
        //            .Add(await _deviceTypeRepository.GetByIdAsync(deviceType.DeviceTypeId));
        //    }
        //    await _detailRepository.CreateAsync(detailEntity);

        //    return CreatedAtRoute("GetDetail", new { id = detailEntity.DetailId }, Mapper.Map<DetailDto>(detailEntity));
        //}

        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteDetail(int id)
        //{
        //    if (!await _detailRepository.ExistsAsync(id))
        //    {
        //        return NotFound();
        //    }

        //    await _detailRepository.DeleteAsync(id);

        //    return NoContent();
        //}
    }
}