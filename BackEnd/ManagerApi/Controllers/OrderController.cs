﻿using AutoMapper;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Dto;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ManagerApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("manager-api/[controller]")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderService  _orderService;

        public OrderController(IOrderRepository orderRepository, IOrderService orderService, IMapper mapper) : base(mapper)
        {
            _orderRepository = orderRepository;
            _orderService = orderService;
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult<int>> GetCount([FromQuery] int orderStatusId)
        {
            var result = await _orderRepository.GetAll(orderStatusId == 0 ? null : o => o.OrderStatusId == orderStatusId).CountAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerOrderDto>>> GetPaged([FromQuery] int count, [FromQuery] int page, [FromQuery] string orderBy, [FromQuery] bool isDescending, [FromQuery] int orderStatusId)
        {
            var orders = await _orderService.GetPagedAsync(orderStatusId == 0 ? null : o => o.OrderStatusId == orderStatusId, count, page, orderBy, isDescending);

            return Ok(Mapper.Map<IEnumerable<ManagerOrderDto>>(orders));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ManagerOrderDetailedDto>> GetOrder(int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ManagerOrderDetailedDto>(order));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int id, ManagerOrderDetailedDto order)
        {
            if (id != order.OrderId)
            {
                return BadRequest();
            }

            var original = await _orderRepository.GetByIdAsync(id);
            var changed = Mapper.Map<Order>(order);

            if (original == null)
            {
                return NotFound();
            }

            if (changed.Employee != null && original.EmployeeId != changed.Employee.UserId)
            {
                original.EmployeeId = changed.Employee.UserId;
                original.OrderStatusId = 2;
            }

            await _orderRepository.UpdateAsync(id, original);

            return NoContent();
        }
    }
}