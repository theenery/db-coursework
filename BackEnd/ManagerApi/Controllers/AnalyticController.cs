﻿using AutoMapper;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Dto;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ManagerApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("manager-api/[controller]")]
    [ApiController]
    public class AnalyticController: BaseController
    {
        private readonly IOrderRepository _orderRepository;

        public AnalyticController(IOrderRepository orderRepository, IMapper mapper) : base(mapper)
        {
            _orderRepository = orderRepository;
        }

        [Route("date-range")]
        [HttpGet]
        public async Task<ActionResult<object>> GetRange()
        {
            return new { 
                Min = await _orderRepository.GetAll().MinAsync(o => o.OrderDate),
                Max = await _orderRepository.GetAll().MaxAsync(o => o.OrderDate)
            };
        }

        [Route("export-data")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManagerOrderDetailedDto>>> GetData([FromQuery] DateTime fromDate, [FromQuery] DateTime toDate)
        {
            var orders = await _orderRepository
                .GetAll(o => o.OrderDate > fromDate && o.OrderDate < toDate)
                .ToListAsync();
            return Ok(Mapper.Map<IEnumerable<ManagerOrderDetailedDto>>(orders));
        }

        [HttpGet]
        public async Task<ActionResult<KeyValuePair<int, decimal>>> GetAnalytics([FromQuery] string type, [FromQuery] DateTime fromDate, [FromQuery] DateTime toDate)
        {
            var aggregatedData = (await _orderRepository
                .GetAll(o => o.OrderDate > fromDate && o.OrderDate < toDate)
                .ToListAsync())
                .GroupBy(o => o.OrderDate.Month, o => o, (month, orders) => new { Key = month, Value = orders.ToList() })
                .ToList();

            var incomes = aggregatedData
                .Select(kv => new
                KeyValuePair<int, decimal> (
                    kv.Key,
                    kv.Value.Sum(o => 
                    {
                        if (type == "services") { 
                            return
                                o.Services.Sum(s => s.ServicePrice) *
                                (decimal)(1.0 - o.OrderDiscountValue);
                        }
                        else if (type == "orderDetails") {
                            return o.OrderDetails.Sum(od => od.DetailCount * od.DetailPrice); 
                        }
                        else { return 0; }
                    })
                ));

            return Ok(incomes);
        }
    }
}
