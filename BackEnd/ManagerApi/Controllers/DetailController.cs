﻿using AutoMapper;
using Infrastructure.Dto;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Extensions;
using Domain.Common;
using Domain.Services.Interfaces;
namespace EmployeeApi.Controllers
{
    [Authorize(Roles = "Manager")]
    [Produces("application/json")]
    [Route("manager-api/[controller]")]
    [ApiController]
    public class DetailController : BaseController
    {
        private readonly IDetailRepository _detailRepository;
        private readonly IDetailService _detailService;
        private readonly IDeviceTypeRepository _deviceTypeRepository;

        public DetailController(IDetailRepository detailRepository, IDetailService detailService, IDeviceTypeRepository deviceTypeRepository, IMapper mapper) : base(mapper)
        {
            _detailRepository = detailRepository;
            _detailService = detailService;
            _deviceTypeRepository = deviceTypeRepository;
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult<int>> GetCount([FromQuery] int deviceType, [FromQuery] string? name)
        {
            var dt = await _deviceTypeRepository.GetByIdAsync(deviceType);
            if (dt == null)
            {
                return NotFound();
            }

            var result = dt.Details
                .Where(d => d.DetailName.Contains(name ?? ""))
                .Count();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DetailDto>>> GetPaged([FromQuery] int deviceType, [FromQuery] string? name, [FromQuery] int count, [FromQuery] int page, [FromQuery] string orderBy, [FromQuery] bool isDescending)
        {
            var dt = await _deviceTypeRepository.GetByIdAsync(deviceType);
            if (dt == null)
            {
                return NotFound();
            }

            var result = await _detailService
                .GetPagedAsync(
                    d => d.DeviceTypes.Contains(dt) && d.DetailName.Contains(name ?? ""),
                    count,
                    page,
                    orderBy,
                    isDescending);
            return Ok(Mapper.Map< IEnumerable<DetailDto>>(result));
        }

        [HttpGet("{id}", Name = "GetDetail")]
        public async Task<ActionResult<DetailWithDeviceTypesDto>> GetDetail(int id)
        {
            var detail = await _detailRepository.GetByIdAsync(id);

            if (detail == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<DetailWithDeviceTypesDto>(detail));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetail(int id, DetailWithDeviceTypesDto detail)
        {
            if (id != detail.DetailId)
            {
                return BadRequest();
            }

            var original = await _detailRepository.GetByIdAsync(id);
            var changed = Mapper.Map<Detail>(detail);

            if (original == null)
            {
                return NotFound();
            }

            original.DeviceTypes.Clear();
            foreach (var deviceType in changed.DeviceTypes)
            {
                original.DeviceTypes
                    .Add(await _deviceTypeRepository.GetByIdAsync(deviceType.DeviceTypeId));
            }
            await _detailRepository.UpdateAsync(id, changed);

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<DetailWithDeviceTypesDto>> PostDetail(DetailWithDeviceTypesDto detail)
        {
            var detailEntity = Mapper.Map<Detail>(detail);
            var deviceTypes = detailEntity.DeviceTypes;
            detailEntity.DeviceTypes = new List<DeviceType>();
            foreach (var deviceType in deviceTypes) {
                detailEntity.DeviceTypes
                    .Add(await _deviceTypeRepository.GetByIdAsync(deviceType.DeviceTypeId));
            }
            await _detailRepository.CreateAsync(detailEntity);

            return CreatedAtRoute("GetDetail", new { id = detailEntity.DetailId }, Mapper.Map<DetailDto>(detailEntity));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDetail(int id)
        {
            if (!await _detailRepository.ExistsAsync(id))
            {
                return NotFound();
            }

            await _detailRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}