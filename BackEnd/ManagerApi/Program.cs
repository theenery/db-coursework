using Domain;

namespace ManagerApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebApplication
                .CreateBuilder(args)
                .ConfigureServices("ManagerApi")
                .Build()
                .Configure()
                .Run();
        }
    }
}
