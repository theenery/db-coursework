using Domain;

namespace MainApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebApplication
                .CreateBuilder(args)
                .ConfigureServices("sa")
                .Build()
                .Configure()
                .Run();
        }
    }
}