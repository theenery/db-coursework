﻿using AutoMapper;
using Domain.Common;
using Infrastructure.Dto;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MainApi.Controllers
{
    [Produces("application/json")]
    [Route("main-api/[controller]")]
    [ApiController]
    public class ServiceController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IDeviceTypeRepository _deviceTypeRepository;

        public ServiceController(IServiceRepository serviceRepository, IDeviceTypeRepository deviceTypeRepository, IMapper mapper) : base(mapper)
        {
            _serviceRepository = serviceRepository;
            _deviceTypeRepository = deviceTypeRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeviceTypeWithServicesDto>>> GetService()
        {
            var dts = (await _deviceTypeRepository.GetAll().ToListAsync()).Where(dt => dt.Services.Count != 0);
            return Ok(Mapper.Map<IEnumerable<DeviceTypeWithServicesDto>>(dts));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ServiceDto>> GetService(int id)
        {
            var service = await _serviceRepository.GetByIdAsync(id);

            if (service == null)
            {
                return NotFound();
            }
            
            return Ok(Mapper.Map<ServiceDto>(service));
        }
    }
}