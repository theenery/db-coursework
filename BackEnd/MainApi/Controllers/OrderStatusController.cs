﻿using AutoMapper;
using Domain.Common;
using Infrastructure.Dto;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MainApi.Controllers
{
    [Produces("application/json")]
    [Route("main-api/[controller]")]
    [ApiController]
    public class OrderStatusController: BaseController
    {
        private readonly IOrderStatusRepository _orderStatusRepository;

        public OrderStatusController(IOrderStatusRepository orderStatusRepository, IMapper mapper): base(mapper) {
            _orderStatusRepository = orderStatusRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderStatusDto>>> GetAll()
        {
            var orderStatuses = await _orderStatusRepository.GetAll().ToListAsync();
            return Ok(Mapper.Map<IEnumerable<OrderStatusDto>>(orderStatuses));
        }
    }
}
