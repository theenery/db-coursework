﻿using AutoMapper;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Dto;
using Microsoft.AspNetCore.Mvc;

namespace MainApi.Controllers
{
    [Produces("application/json")]
    [Route("main-api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService, IMapper mapper) : base(mapper)
        {
            _accountService = accountService;
        }

        [HttpPut]
        public async Task<IActionResult> RegisterUser(RegisterUserDto user)
        {
            if (await _accountService.RegisterUser(user))
            {
                return NoContent();
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> LoginUser(LoginUserDto user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            if (await _accountService.LoginUser(user))
            {
                var identityUser = await _accountService.FindByEmailAsync(user.Email);
                var result = new
                {
                    identityUser.Id,
                    FirstName = identityUser.FirstName,
                    LastName = identityUser.LastName,
                    Role = await _accountService.GetRoleAsync(identityUser),
                    Token = await _accountService.GenerateTokenStringAsync(identityUser),
                };
                return Ok(result);
            }

            return BadRequest();
        }
    }
}
