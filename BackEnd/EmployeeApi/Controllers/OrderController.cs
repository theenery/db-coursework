﻿using AutoMapper;
using Castle.Core.Internal;
using Domain.Common;
using Domain.Services.Interfaces;
using Infrastructure.Dto;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeeApi.Controllers
{
    [Authorize(Roles = "Employee")]
    [Produces("application/json")]
    [Route("employee-api/employees/{employee}/[controller]")]
    [ApiController]
    public class OrderController : BaseController
    {
        private readonly IDetailRepository _detailRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderService  _orderService;
        private readonly IServiceRepository _serviceRepository;

        public OrderController(IDetailRepository detailRepository, IDeviceRepository deviceRepository, IOrderRepository orderRepository, IOrderService orderService, IServiceRepository serviceRepository, IMapper mapper) : base(mapper)
        {
            _detailRepository = detailRepository;
            _deviceRepository = deviceRepository;
            _orderRepository = orderRepository;
            _orderService = orderService;
            _serviceRepository = serviceRepository;
        }

        [Route("count")]
        [HttpGet]
        public async Task<ActionResult<int>> GetCount(int employee)
        {
            var result = await _orderRepository.GetAll(o => o.EmployeeId == employee).CountAsync();
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeOrderDto>>> GetPaged(int employee, [FromQuery] int count, [FromQuery] int page, [FromQuery] string orderBy, [FromQuery] bool isDescending)
        {
            var orders = await _orderService.GetPagedAsync(o => o.EmployeeId == employee, count, page, orderBy, isDescending);

            return Ok(Mapper.Map<IEnumerable<EmployeeOrderDto>>(orders));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeOrderDetailedDto>> GetOrder(int employee, int id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null || order.EmployeeId != employee)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<EmployeeOrderDetailedDto>(order));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder(int employee, int id, EmployeeOrderDetailedDto order)
        {
            if (id != order.OrderId)
            {
                return BadRequest();
            }

            var original = await _orderRepository.GetByIdAsync(id);
            var changed = Mapper.Map<Order>(order);

            if (original == null)
            {
                return NotFound();
            }

            original.OrderStatusId = changed.OrderStatus.OrderStatusId;
            original.OrderStatus = null;

            if(!changed.Device.DeviceModel.IsNullOrEmpty())
            {
                if (original.Device != null)
                {
                    original.Device.DeviceMaker = changed.Device.DeviceMaker;
                    original.Device.DeviceModel = changed.Device.DeviceModel;
                }
                else
                {
                    await _deviceRepository.CreateAsync(changed.Device!);
                    original.DeviceId = changed.Device.DeviceId;
                    original.Device = null;
                }
            }
            if (original.Services != null)
            {
                original.Services.Clear();
            }
            foreach (var service in order.Services)
            {
                original.Services.Add(await _serviceRepository.GetByIdAsync(service.ServiceId));
            }

            var details = new List<OrderDetail>(original.OrderDetails);
            if (original.OrderDetails != null)
            {
                original.OrderDetails.Clear();
            }
            foreach (var newDetail in changed.OrderDetails)
            {
                var oldDetail = details.SingleOrDefault(d => d.DetailId == newDetail.DetailId);
                newDetail.OrderId = original.OrderId;
                if (oldDetail == null) {
                    if ((await _detailRepository.GetByIdAsync(newDetail.DetailId)).DetailCount >= newDetail.DetailCount) {
                        newDetail.OrderDetailStatusId = 3;
                    } 
                    else
                    {
                        newDetail.OrderDetailStatusId = 1;
                    }
                    newDetail.OrderDetailStatus = null;
                    original.OrderDetails.Add(newDetail);
                }
                else
                {
                    if ((await _detailRepository.GetByIdAsync(newDetail.DetailId)).DetailCount >= newDetail.DetailCount)
                    {
                        if (newDetail.OrderDetailStatus.OrderDetailStatusId != 1)
                        {
                            oldDetail.OrderDetailStatusId = newDetail.OrderDetailStatus.OrderDetailStatusId;
                            if (newDetail.OrderDetailStatus.OrderDetailStatusId == 4)
                            {
                                (await _detailRepository.GetByIdAsync(newDetail.DetailId)).DetailCount -= newDetail.DetailCount;
                            }
                        }
                        else
                        {
                            oldDetail.OrderDetailStatusId = 3;
                        }
                    }
                    else
                    {
                        oldDetail.OrderDetailStatusId = 1;
                    }
                    //oldDetail.OrderDetailStatus = null;
                    oldDetail.DetailCount = newDetail.DetailCount;
                    oldDetail.DetailPrice = newDetail.DetailPrice;
                    original.OrderDetails.Add(oldDetail);
                }
            }

            if (original.OrderStatusId != 4)
            {
                if (original.OrderDetails.Any(od => od.OrderDetailStatusId <= 2))
                {
                    original.OrderStatusId = 3;
                    original.OrderStatus = null;
                }
                else
                {
                    original.OrderStatusId = 2;
                    original.OrderStatus = null;
                }
            }

            await _orderRepository.UpdateAsync(id, original);

            return NoContent();
        }
    }
}