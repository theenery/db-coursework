using Domain;

namespace EmployeeApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            WebApplication
                .CreateBuilder(args)
                .ConfigureServices("EmployeeApi")
                .Build()
                .Configure()
                .Run();
        }
    }
}
