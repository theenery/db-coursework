﻿namespace Infrastructure.Dto
{
    public class ClientOrderDto
    {
        public int OrderId { get; set; }
        public DeviceTypeDto DeviceType { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        public OrderStatusDto OrderStatus { get; set; } = null!;
        public int OrderPrice { get; set; }
    }
}
