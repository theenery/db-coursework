﻿namespace Infrastructure.Dto
{
    public class DeviceTypeWithServicesDto
    {
        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; } = null!;

        public ICollection<ServiceDto> Services { get; set; } = new HashSet<ServiceDto>();
    }
}
