﻿namespace Infrastructure.Dto
{
    public class EmployeeOrderDetailedDto
    {
        public EmployeeOrderDetailedDto()
        {
            Services = new HashSet<ServiceDto>();
            OrderDetails = new HashSet<OrderDetailDto>();
        }

        public int OrderId { get; set; }
        public ClientAsUserDto Client { get; set; } = null!;
        public DeviceDto? Device { get; set; }
        public DeviceTypeDto DeviceType { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        public string OrderDescription { get; set; } = null!;
        public OrderStatusDto OrderStatus { get; set; } = null!;
        public ICollection<ServiceDto> Services { get; set; }
        public ICollection<OrderDetailDto> OrderDetails { get; set; }
    }
}
