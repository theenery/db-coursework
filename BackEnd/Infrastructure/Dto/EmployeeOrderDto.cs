﻿namespace Infrastructure.Dto
{
    public class EmployeeOrderDto
    {
        public int OrderId { get; set; }
        public DeviceDto? Device { get; set; }
        public DeviceTypeDto DeviceType { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        public string OrderDescription { get; set; } = null!;
        public OrderStatusDto OrderStatus { get; set; } = null!;
    }
}
