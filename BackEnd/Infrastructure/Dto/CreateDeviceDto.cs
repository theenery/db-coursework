﻿namespace Infrastructure.Dto
{
    public class CreateDeviceDto
    {
        public int DeviceTypeId { get; set; }
        public string DeviceMaker { get; set; } = null!;
        public string? DeviceModel { get; set; }
    }
}
