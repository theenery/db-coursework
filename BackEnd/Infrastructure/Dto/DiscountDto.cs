﻿namespace Infrastructure.Dto
{
    public class DiscountDto
    {
        public int DiscountId { get; set; }
        public DateTime DiscountStarting { get; set; }
        public DateTime DiscountExpiring { get; set; }
        public double DiscountValue { get; set; }
    }
}
