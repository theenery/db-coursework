﻿namespace Infrastructure.Dto
{
    public class ClientOrderDetailedDto
    {
        public ClientOrderDetailedDto() { 
            Services = new HashSet<ServiceDto>();
            OrderDetails = new HashSet<OrderDetailDto>();
        }

        public int OrderId { get; set; }
        public EmployeeAsUserDto? Employee { get; set; }
        public DeviceDto? Device { get; set; }
        public DeviceTypeDto DeviceType { get; set; } = null!;
        public double OrderDiscountValue { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderDescription { get; set; } = null!;
        public OrderStatusDto OrderStatus { get; set; } = null!;
        public ICollection<ServiceDto> Services { get; set; }
        public ICollection<OrderDetailDto> OrderDetails { get; set; }
    }
}
