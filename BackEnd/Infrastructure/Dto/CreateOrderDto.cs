﻿namespace Infrastructure.Dto
{
    public class CreateOrderDto
    {
        public int DeviceTypeId { get; set; }
        public string OrderDescription { get; set; } = null!;
    }
}
