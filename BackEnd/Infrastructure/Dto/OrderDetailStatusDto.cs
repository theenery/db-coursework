﻿namespace Infrastructure.Dto
{
    public class OrderDetailStatusDto
    {
        public int OrderDetailStatusId { get; set; }
        public string OrderDetailStatusName { get; set; } = null!;
    }
}
