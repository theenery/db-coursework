﻿namespace Infrastructure.Dto
{
    public class DetailWithDeviceTypesDto
    {
        public DetailWithDeviceTypesDto() { 
            DeviceTypes = new HashSet<DeviceTypeDto>();
        }

        public int DetailId { get; set; }
        public string DetailName { get; set; } = null!;
        public decimal? DetailPrice { get; set; }
        public int DetailCount { get; set; }
        public ICollection<DeviceTypeDto> DeviceTypes { get; set; }
    }
}
