﻿namespace Infrastructure.Dto
{
    public class UserWithContactsDto
    {
        public string Email { get; set; } = null!;
        public string? PhoneNumber { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
    }
}
