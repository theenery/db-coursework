﻿namespace Infrastructure.Dto
{
    public class ManagerOrderDetailedDto
    {
        public ManagerOrderDetailedDto()
        {
            Services = new HashSet<ServiceDto>();
            OrderDetails = new HashSet<OrderDetailDto>();
        }

        public int OrderId { get; set; }
        public ClientAsUserDto Client { get; set; } = null!;
        public EmployeeDto? Employee { get; set; }
        public DeviceDto? Device { get; set; }
        public DeviceTypeDto DeviceType { get; set; } = null!;
        public DateTime OrderDate { get; set; }
        public string OrderDescription { get; set; } = null!;
        public OrderStatusDto OrderStatus { get; set; } = null!;
        public ICollection<ServiceDto> Services { get; set; }
        public ICollection<OrderDetailDto> OrderDetails { get; set; }
        public decimal OrderPrice { get; set; }
        public double OrderDiscountValue { get; set; }
    }
}
