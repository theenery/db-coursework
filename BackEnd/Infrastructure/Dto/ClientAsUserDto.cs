﻿namespace Infrastructure.Dto
{
    public class ClientAsUserDto
    {
        public UserWithContactsDto User { get; set; } = null!;
    }
}
