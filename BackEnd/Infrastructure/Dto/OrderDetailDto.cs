﻿namespace Infrastructure.Dto
{
    public class OrderDetailDto
    {
        public int OrderId { get; set; }
        public int DetailId { get; set; }
        public int DetailCount { get; set; }
        public string DetailName { get; set; } = null!;
        public decimal DetailPrice { get; set; }
        public OrderDetailStatusDto OrderDetailStatus { get; set; } = null!;
    }
}
