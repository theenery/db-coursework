﻿namespace Infrastructure.Dto
{
    public class EmployeeDto
    {
        public int UserId { get; set; }
        public string EmployeeJob { get; set; } = null!;
        public UserDto User { get; set; } = null!;
    }
}
