﻿using Infrastructure.Repository.Implementations;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Extensions
{
    public static class RepositoryDiExtensions
    {
        public static WebApplicationBuilder AddRepositoryDi(this WebApplicationBuilder builder)
        {
            builder.Services
                .AddTransient<IDetailRepository, DetailRepository>()
                .AddTransient<IDeviceRepository, DeviceRepository>()
                .AddTransient<IDeviceTypeRepository, DeviceTypeRepository>()
                .AddTransient<IEmployeeRepository, EmployeeRepository>()
                .AddTransient<IOrderRepository, OrderRepository>()
                .AddTransient<IServiceRepository, ServiceRepository>()
                .AddTransient<IOrderStatusRepository, OrderStatusRepository>()
                .AddTransient<IOrderDetailRepository, OrderDetailRepository>();

            return builder;
        }
    }
}
