﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Extensions
{
    public static class IQueriableExtentions
    {
        public static async Task<List<T>> GetPagedAsync<T>(this IQueryable<T> queryable, int count, int page) where T : class
        {
            return await queryable.Skip(count * page).Take(count).ToListAsync();
        }
    }
}
