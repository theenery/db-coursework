﻿using Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Extensions
{
    public static class MapperExtensions
    {
        public static WebApplicationBuilder AddAutoMapperService(this WebApplicationBuilder builder)
        {
            builder.Services.AddAutoMapper(cfg => cfg.AddProfile<MappingProfile>());

            return builder;
        }
    }
}
