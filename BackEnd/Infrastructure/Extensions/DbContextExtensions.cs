﻿using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Infrastructure.Models;

namespace Infrastructure.Extensions
{
    public static class DbContextExtensions
    {
        public static string GetConnectionStringService(this IConfiguration configuration, string user) { 
            return configuration
                .GetConnectionString("DefaultConnection")
                .Replace("<user>", user)
                .Replace("<password>", configuration.GetSection($"ConnectionCredentials:{user}").Value);
        }

        public static IServiceCollection AddDbContextService(this IServiceCollection services, string connectionString) { 
            services.AddDbContext<ApiDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
                options.UseLazyLoadingProxies();
            });

            return services;
        }

        public static WebApplicationBuilder AddDbContextService(this WebApplicationBuilder builder, string user)
        {
            var connectionString = builder.Configuration.GetConnectionStringService(user);

            builder.Services.AddDbContextService(connectionString);

            return builder;
        }
    }
}
