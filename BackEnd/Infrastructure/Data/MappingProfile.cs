﻿using AutoMapper;
using Infrastructure.Dto;
using Infrastructure.Models;


namespace Infrastructure.Data
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateServiceDto, Service>();
            CreateMap<Service, CreateServiceDto>();
            CreateMap<ServiceDto, Service>();
            CreateMap<Service, ServiceDto>();

            CreateMap<DetailWithDeviceTypesDto, Detail>();
            CreateMap<Detail, DetailWithDeviceTypesDto>();
            CreateMap<DetailDto, Detail>();
            CreateMap<Detail, DetailDto>();

            CreateMap<CreateDeviceDto, Device>();
            CreateMap<Device, CreateDeviceDto>();
            CreateMap<DeviceDto, Device>();
            CreateMap<Device, DeviceDto>();

            CreateMap<EmployeeDto, Employee>();
            CreateMap<Employee, EmployeeDto>();

            CreateMap<CreateOrderDto, Order>()
                .ForMember(dto => dto.OrderDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dto => dto.OrderStatusId, opt => opt.MapFrom(src => 1));

            CreateMap<ClientOrderDto, Order>();
            CreateMap<Order, ClientOrderDto>();
            CreateMap<ClientOrderDetailedDto, Order>();
            CreateMap<Order, ClientOrderDetailedDto>();

            CreateMap<EmployeeOrderDto, Order>();
            CreateMap<Order, EmployeeOrderDto>();
            CreateMap<EmployeeOrderDetailedDto, Order>();
            CreateMap<Order, EmployeeOrderDetailedDto>();

            CreateMap<ManagerOrderDto, Order>();
            CreateMap<Order, ManagerOrderDto>();
            CreateMap<ManagerOrderDetailedDto, Order>();
            CreateMap<Order, ManagerOrderDetailedDto>();

            CreateMap<Employee, EmployeeAsUserDto>();
            CreateMap<EmployeeAsUserDto, Employee>();
            CreateMap<Client, ClientAsUserDto>();
            CreateMap<ClientAsUserDto, Client>();

            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<User, UserWithContactsDto>();
            CreateMap<UserWithContactsDto, User>();

            CreateMap<OrderDetail, OrderDetailDto>()
                .ForMember(odd => odd.DetailName, opt => opt.MapFrom(od => od.Detail.DetailName));
            CreateMap<OrderDetailDto, OrderDetail>();

            CreateMap<Discount, DiscountDto>();
            CreateMap<DiscountDto, Discount>();

            CreateMap<OrderDetailStatus, OrderDetailStatusDto>();
            CreateMap<OrderDetailStatusDto, OrderDetailStatus>();

            CreateMap<DeviceTypeWithServicesDto, DeviceType>();
            CreateMap<DeviceType, DeviceTypeWithServicesDto>();
            CreateMap<DeviceTypeDto, DeviceType>();
            CreateMap<DeviceType, DeviceTypeDto>();

            CreateMap<OrderStatusDto, OrderStatus>();
            CreateMap<OrderStatus, OrderStatusDto>();
            //.ForMember(dto => dto.SomeProm, opt => opt.MapFrom(src => src.OtherProp))
        }
    }
}
