﻿using Infrastructure.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class ApiDbContext : IdentityDbContext<User, Role, int>
    {
        public ApiDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Client> Clients { get; set; } = null!;
        public DbSet<Detail> Details { get; set; } = null!;
        public DbSet<Device> Devices { get; set; } = null!;
        public DbSet<DeviceType> DeviceTypes { get; set; } = null!;
        public DbSet<Discount> Discounts { get; set; } = null!;
        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Order> Orders { get; set; } = null!;
        public DbSet<OrderDetail> OrderDetails { get; set; } = null!;
        public DbSet<OrderDetailStatus> OrderDetailStatuses { get; set; } = null!;
        public DbSet<OrderStatus> OrderStatuses { get; set; } = null!;
        public DbSet<Review> Reviews { get; set; } = null!;
        public DbSet<Service> Services { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientAdress)
                    .HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Client)
                    .HasForeignKey<Client>(d => d.UserId);
            });

            modelBuilder.Entity<Detail>(entity =>
            {
                entity.Property(e => e.DetailId);

                entity.Property(e => e.DetailCount);

                entity.Property(e => e.DetailName)
                    .HasMaxLength(64);

                entity.Property(e => e.DetailPrice)
                    .HasColumnType("money");

                entity.HasMany(d => d.DeviceTypes)
                    .WithMany(p => p.Details);
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.Property(e => e.DeviceId);

                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.DeviceMaker)
                    .HasMaxLength(24);

                entity.Property(e => e.DeviceModel)
                    .HasMaxLength(24);

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<DeviceType>(entity =>
            {
                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.DeviceTypeName);

                entity.HasMany(d => d.Discounts)
                    .WithMany(p => p.DeviceTypes);

                entity.HasMany(d => d.Details)
                    .WithMany(p => p.DeviceTypes);

                entity.HasMany(d => d.Services)
                    .WithOne(p => p.DeviceType);

                entity.HasMany(d => d.Orders)
                    .WithOne(p => p.DeviceType);
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.Property(e => e.DiscountId);

                entity.Property(e => e.DiscountExpiring)
                    .HasColumnType("date");

                entity.Property(e => e.DiscountStarting)
                    .HasColumnType("date");

                entity.Property(e => e.DiscountValue);

                entity.HasMany(d => d.Clients)
                    .WithMany(p => p.Discounts);

                entity.HasMany(d => d.DeviceTypes)
                    .WithMany(p => p.Discounts);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.HasIndex(e => e.EmployeePass)
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever();

                entity.Property(e => e.EmployeePass)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EmployeeJob);

                entity.Property(e => e.EmployeeSalary)
                    .HasColumnType("money");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Employee)
                    .HasForeignKey<Employee>(d => d.UserId);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.OrderId);

                entity.Property(e => e.ClientId);

                entity.Property(e => e.EmployeeId);

                entity.Property(e => e.DeviceId);

                entity.Property(e => e.OrderDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderStatusId);

                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.OrderDescription);

                entity.Property(e => e.OrderPrice)
                    .HasComputedColumnSql("0");

                entity.Property(e => e.OrderDiscountValue)
                    .HasComputedColumnSql("0");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientCascade);

                entity.HasOne(d => d.Device)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.DeviceId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.OrderStatus)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.OrderStatusId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(d => d.Services)
                    .WithMany(p => p.Orders);

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.DetailId, e.OrderId });

                entity.Property(e => e.DetailId);

                entity.Property(e => e.OrderId);

                entity.Property(e => e.DetailCount);

                entity.Property(e => e.DetailPrice)
                    .HasColumnType("money");

                entity.Property(e => e.OrderDetailStatusId);

                entity.HasOne(d => d.OrderDetailStatus)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderDetailStatusId)
                    .OnDelete(DeleteBehavior.NoAction);

                entity.HasOne(d => d.Detail)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.DetailId)
                    .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<OrderDetailStatus>(entity =>
            {
                entity.Property(e => e.OrderDetailStatusId);

                entity.Property(e => e.OrderDetailStatusName)
                    .HasMaxLength(24);

                entity.HasMany(d => d.OrderDetails)
                    .WithOne(p => p.OrderDetailStatus);
            });

            modelBuilder.Entity<OrderStatus>(entity =>
            {
                entity.Property(e => e.OrderStatusId);

                entity.Property(e => e.OrderStatusName)
                    .HasMaxLength(24);

                entity.HasMany(d => d.Orders)
                    .WithOne(p => p.OrderStatus);
            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.Property(e => e.ReviewId);

                entity.Property(e => e.ClientId);

                entity.Property(e => e.ReviewDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ReviewRate);

                entity.Property(e => e.ReviewText)
                    .HasColumnType("ntext");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Reviews)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.Property(e => e.ServiceId);

                entity.Property(e => e.DeviceTypeId);

                entity.Property(e => e.ServiceName)
                    .HasMaxLength(32);

                entity.Property(e => e.ServicePrice)
                    .HasColumnType("money");

                entity.HasOne(s => s.DeviceType)
                    .WithMany(d => d.Services)
                    .HasForeignKey(s => s.DeviceTypeId)
                    .OnDelete(DeleteBehavior.NoAction);

                entity.HasMany(s => s.Orders)
                    .WithMany(o => o.Services);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.FirstName)
                    .HasMaxLength(24);

                entity.Property(e => e.LastName)
                    .HasMaxLength(24);

                entity.HasOne(d => d.Client)
                    .WithOne(p => p.User);

                entity.HasOne(d => d.Employee)
                    .WithOne(p => p.User);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
