﻿namespace Infrastructure.Models
{
    public class OrderDetailStatus
    {
        public OrderDetailStatus()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderDetailStatusId { get; set; }
        public string OrderDetailStatusName { get; set; } = null!;

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
