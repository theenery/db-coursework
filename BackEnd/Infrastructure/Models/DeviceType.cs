﻿namespace Infrastructure.Models
{
    public partial class DeviceType
    {
        public DeviceType()
        {
            Discounts = new HashSet<Discount>();
            Details = new HashSet<Detail>();
            Devices = new HashSet<Device>();
            Services = new HashSet<Service>();
            Orders = new HashSet<Order>();
        }
        public int DeviceTypeId { get; set; }
        public string DeviceTypeName { get; set; } = null!;

        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Detail> Details { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
