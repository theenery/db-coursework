﻿namespace Infrastructure.Models
{
    public partial class OrderDetail
    {
        public int DetailId { get; set; }
        public int OrderId { get; set; }
        public int DetailCount { get; set; }
        public decimal DetailPrice { get; set; }
        public int OrderDetailStatusId { get; set; }

        public virtual Detail Detail { get; set; } = null!;
        public virtual Order Order { get; set; } = null!;
        public virtual OrderDetailStatus OrderDetailStatus { get; set; } = null!;
    }
}
