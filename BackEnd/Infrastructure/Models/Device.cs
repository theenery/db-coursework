﻿namespace Infrastructure.Models
{
    public partial class Device
    {
        public Device()
        {
            Orders = new HashSet<Order>();
        }

        public int DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string DeviceMaker { get; set; } = null!;
        public string? DeviceModel { get; set; }

        public virtual DeviceType DeviceType { get; set; } = null!;
        public virtual ICollection<Order> Orders { get; set; }
    }
}
