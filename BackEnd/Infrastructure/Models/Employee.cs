﻿namespace Infrastructure.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Orders = new HashSet<Order>();
        }

        public int UserId { get; set; }
        public string EmployeePass { get; set; } = null!;
        public string EmployeeJob { get; set; } = null!;
        public decimal EmployeeSalary { get; set; }

        public virtual User User { get; set; } = null!;
        public virtual ICollection<Order> Orders { get; set; }
    }
}
