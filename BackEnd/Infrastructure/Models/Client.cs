﻿namespace Infrastructure.Models
{
    public partial class Client
    {
        public Client()
        {
            Discounts = new HashSet<Discount>();
            Orders = new HashSet<Order>();
            Reviews = new HashSet<Review>();
        }

        public int UserId { get; set; }
        public string? ClientAdress { get; set; }

        public virtual User User { get; set; } = null!;
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
