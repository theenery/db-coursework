﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Infrastructure.Models
{
    public partial class Order
    {
        public Order()
        {
            Services = new HashSet<Service>();
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public int ClientId { get; set; }
        public int? DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderDescription { get; set; } = null!;
        public int OrderStatusId { get; set; }
        public decimal OrderPrice {  get; set; }
        public double OrderDiscountValue { get; set; }

        public virtual Client Client { get; set; } = null!;
        public virtual Device? Device { get; set; }
        public virtual DeviceType DeviceType { get; set; } = null!;
        public virtual Employee? Employee { get; set; }
        public virtual OrderStatus OrderStatus { get; set; } = null!;
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
