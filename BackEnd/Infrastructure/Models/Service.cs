﻿namespace Infrastructure.Models
{
    public partial class Service
    {
        public Service()
        {
            Orders = new HashSet<Order>();
        }

        public int ServiceId { get; set; }
        public int DeviceTypeId { get; set; }
        public string ServiceName { get; set; } = null!;
        public decimal ServicePrice { get; set; }

        public virtual DeviceType DeviceType { get; set; } = null!;
        public virtual ICollection<Order> Orders { get; set; }
    }
}
