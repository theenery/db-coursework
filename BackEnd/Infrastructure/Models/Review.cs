﻿namespace Infrastructure.Models
{
    public partial class Review
    {
        public int ReviewId { get; set; }
        public int ClientId { get; set; }
        public byte ReviewRate { get; set; }
        public string? ReviewText { get; set; }
        public DateTime ReviewDate { get; set; }

        public virtual Client Client { get; set; } = null!;
    }
}
