﻿using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Models
{
    public partial class User : IdentityUser<int>
    {
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;

        public virtual Client? Client { get; set; }
        public virtual Employee? Employee { get; set; }
    }
}
