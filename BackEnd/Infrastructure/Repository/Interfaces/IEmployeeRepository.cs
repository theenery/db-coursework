﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {

    }
}
