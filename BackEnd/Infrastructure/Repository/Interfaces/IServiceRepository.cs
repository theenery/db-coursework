﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IServiceRepository : IGenericRepository<Service>
    {

    }
}
