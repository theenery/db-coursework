﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IDeviceTypeRepository : IGenericRepository<DeviceType>
    {

    }
}
