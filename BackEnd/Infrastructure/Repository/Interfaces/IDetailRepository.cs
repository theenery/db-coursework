﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IDetailRepository : IGenericRepository<Detail>
    {

    }
}

