﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IOrderStatusRepository: IGenericRepository<OrderStatus>
    {
    }
}
