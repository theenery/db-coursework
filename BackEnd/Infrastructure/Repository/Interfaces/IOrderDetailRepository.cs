﻿using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository.Interfaces
{
    public interface IOrderDetailRepository: IGenericRepository<OrderDetail>
    {
        public Task UpdateAsync(int detailId, int orderId, OrderDetail entity);
    }
}
