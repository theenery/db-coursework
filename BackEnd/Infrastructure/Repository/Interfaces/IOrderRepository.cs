﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IOrderRepository : IGenericRepository<Order>
    {

    }
}