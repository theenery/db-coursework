﻿using Infrastructure.Models;

namespace Infrastructure.Repository.Interfaces
{
    public interface IDeviceRepository : IGenericRepository<Device>
    {

    }
}
