﻿using Infrastructure.Data;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;

namespace Infrastructure.Repository.Implementations
{
    public class OrderStatusRepository: GenericRepository<OrderStatus>, IOrderStatusRepository
    {
        public OrderStatusRepository(ApiDbContext context) : base(context)
        {

        }
    }
}
