﻿using Infrastructure.Data;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository.Implementations
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(ApiDbContext context) : base(context)
        {

        }
        
        public virtual async Task UpdateAsync(int detailId, int orderId, OrderDetail entity)
        {

            var result = await _dbSet.FindAsync(detailId, orderId) ?? throw new Exception("Entity with id was not found");
            _context.Entry(result).CurrentValues.SetValues(entity);
            await _context.SaveChangesAsync();
        }
    }
}