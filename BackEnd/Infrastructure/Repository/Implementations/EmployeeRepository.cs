﻿using Infrastructure.Data;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Repository.Implementations
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        private readonly UserManager<User> _userManager;

        public EmployeeRepository(ApiDbContext context, UserManager<User> userManager) : base(context)
        {
            _userManager = userManager;
        }

        public override async Task CreateAsync(Employee entity)
        {
            await base.CreateAsync(entity);
            var user = await _userManager.FindByIdAsync(entity.UserId.ToString());
            await _userManager.AddToRoleAsync(user, "Employee");
        }

        public override async Task DeleteAsync(int id)
        {
            await base.DeleteAsync(id);
            var user = await _userManager.FindByIdAsync(id.ToString());
            await _userManager.RemoveFromRoleAsync(user, "Employee");
        }
    }
}
