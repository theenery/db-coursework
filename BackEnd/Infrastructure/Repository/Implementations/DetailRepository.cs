﻿using Infrastructure.Data;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;

namespace Infrastructure.Repository.Implementations
{
    public class DetailRepository : GenericRepository<Detail>, IDetailRepository
    {
        public DetailRepository(ApiDbContext context) : base(context)
        {

        }
    }
}
