﻿using System.Linq.Expressions;

namespace Infrastructure.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        Task CreateAsync(T entity);
        Task DeleteAsync(int id);
        Task<bool> ExistsAsync(int id);
        IQueryable<T> GetAll(Expression<Func<T, bool>>? filter = null);
        Task<T?> GetByIdAsync(int id);
        Task UpdateAsync(int id, T entity);
    }
}