﻿using Domain.Extentions;
using Infrastructure.Data;
using Infrastructure.Extensions;
using Infrastructure.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DbSeeder
{
    public class Program
    {
        static readonly public string ClientRoleName = "Client";
        static readonly public string EmployeeRoleName = "Employee";
        static readonly public string ManagerRoleName = "Manager";

        static private string RoleToDbUser(string role) => role + "Api";

        static public string ClientDbUserName => RoleToDbUser(ClientRoleName);
        static public string EmployeeDbUserName => RoleToDbUser(EmployeeRoleName);
        static public string ManagerDbUserName => RoleToDbUser(ManagerRoleName);

        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddSharedSettings()
                .AddEnvironmentVariables()
                .Build();

            var connectionString = configuration.GetConnectionStringService("sa");

            var serviceProvider = new ServiceCollection()
                .AddLogging(builder => builder.AddConsole())
                .AddDbContextService(connectionString)
                .AddAuthService(configuration)
                .AddSingleton(configuration)
                .BuildServiceProvider();

            SeedDatabase(serviceProvider, configuration);
        }

        private static void SeedDatabase(ServiceProvider services, IConfiguration configuration)
        {
            var context = services.GetRequiredService<ApiDbContext>();

            EnsureRolesDeleted(services);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            ConfigureRoles(services, configuration);

            ConfigureProcedures(services);
            ConfigureComputedColumns(services);

            SeedData(services);
        }

        private static void ConfigureComputedColumns(IServiceProvider services)
        {
            var context = services.GetRequiredService<ApiDbContext>();

            context.Database.ExecuteSqlRaw(@"
                ALTER TABLE Orders
                DROP COLUMN OrderPrice;
                ALTER TABLE Orders
                ADD OrderPrice AS dbo.GetFinalPrice(OrderId);
            ");

            context.Database.ExecuteSqlRaw(@"
                ALTER TABLE Orders
                DROP COLUMN OrderDiscountValue;
                ALTER TABLE Orders
                ADD OrderDiscountValue AS dbo.GetDiscountValue(OrderId);
            ");
        }

        private static void EnsureRolesDeleted(IServiceProvider services)
        {
            var context = services.GetRequiredService<ApiDbContext>();

            static string deleteSql(string roleName)
            {
                string dbUserName = RoleToDbUser(roleName);

                return $@"
                    IF EXISTS (SELECT * FROM sys.database_principals WHERE name = '{dbUserName}')
                        DROP USER [{dbUserName}];
                    IF EXISTS (SELECT * FROM sys.database_principals WHERE name = '{roleName}')
                        DROP ROLE [{roleName}];
                    IF EXISTS (SELECT * FROM sys.server_principals WHERE name = '{dbUserName}')
                        DROP LOGIN [{dbUserName}];
                ";
            }

            context.Database.ExecuteSqlRaw(deleteSql(ClientRoleName));
            context.Database.ExecuteSqlRaw(deleteSql(EmployeeRoleName));
            context.Database.ExecuteSqlRaw(deleteSql(ManagerRoleName));
        }

        private static void ConfigureRoles(IServiceProvider services, IConfiguration configuration)
        {
            var context = services.GetRequiredService<ApiDbContext>();

            // create roles

            context.Database.ExecuteSqlRaw($@"
                CREATE ROLE [{ClientRoleName}];
                GRANT SELECT ON [dbo].[AspNetUsers] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Clients] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[ClientDiscount] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Details] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Devices] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[DeviceTypes] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[DeviceTypeDiscount] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Discounts] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Employees] TO [{ClientRoleName}];
                GRANT SELECT, INSERT ON [dbo].[Orders] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[OrderDetails] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[OrderDetailStatuses] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[OrderService] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[OrderStatuses] TO [{ClientRoleName}];
                GRANT SELECT ON [dbo].[Services] TO [{ClientRoleName}];
            ");

            context.Database.ExecuteSqlRaw($@"
                CREATE ROLE [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[AspNetUsers] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[Clients] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[ClientDiscount] TO [{EmployeeRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Details] TO [{EmployeeRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[DetailDeviceType] TO [{EmployeeRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Devices] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[DeviceTypes] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[DeviceTypeDiscount] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[Discounts] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[Employees] TO [{EmployeeRoleName}];
                GRANT SELECT, UPDATE ON [dbo].[Orders] TO [{EmployeeRoleName}];
                GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[OrderDetails] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[OrderDetailStatuses] TO [{EmployeeRoleName}];
                GRANT SELECT, INSERT, DELETE ON [dbo].[OrderService] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[OrderStatuses] TO [{EmployeeRoleName}];
                GRANT SELECT ON [dbo].[Services] TO [{EmployeeRoleName}];
            ");

            context.Database.ExecuteSqlRaw($@"
                CREATE ROLE [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[AspNetUsers] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[Clients] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[ClientDiscount] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Details] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[DetailDeviceType] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Devices] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[DeviceTypes] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[DeviceTypeDiscount] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[Discounts] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[Employees] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Orders] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[OrderDetails] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[OrderDetailStatuses] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, DELETE ON [dbo].[OrderService] TO [{ManagerRoleName}];
                GRANT SELECT ON [dbo].[OrderStatuses] TO [{ManagerRoleName}];
                GRANT SELECT, INSERT, UPDATE ON [dbo].[Services] TO [{ManagerRoleName}];
            ");

            // create users

            string createUserSql(string roleName)
            {
                string dbUserName = RoleToDbUser(roleName);
                string password = configuration.GetSection($"ConnectionCredentials:{dbUserName}").Value;

                return $@"
                    CREATE LOGIN [{dbUserName}] WITH PASSWORD = '{password}';
                    CREATE USER [{dbUserName}] FOR LOGIN [{dbUserName}];
                    ALTER ROLE [{roleName}] ADD MEMBER [{dbUserName}];
                ";
            }

            context.Database.ExecuteSqlRaw(createUserSql(ClientRoleName));
            context.Database.ExecuteSqlRaw(createUserSql(EmployeeRoleName));
            context.Database.ExecuteSqlRaw(createUserSql(ManagerRoleName));
        }

        private static void ConfigureProcedures(IServiceProvider services)
        {
            var context = services.GetRequiredService<ApiDbContext>();

            context.Database.ExecuteSqlRaw(@"
                CREATE FUNCTION GetDiscountValue(@OrderId INT)
                RETURNS FLOAT
                AS BEGIN
                    RETURN COALESCE((
                        SELECT TOP 1 d.DiscountValue
                        FROM Orders AS o
		                LEFT JOIN ClientDiscount AS cd ON cd.ClientsUserId = o.ClientId
		                LEFT JOIN DeviceTypeDiscount AS dtd ON dtd.DeviceTypesDeviceTypeId = o.DeviceTypeId
                        INNER JOIN Discounts AS d ON d.DiscountId = cd.DiscountsDiscountId OR d.DiscountId = dtd.DiscountsDiscountId
                        WHERE o.OrderId = @OrderId 
                            AND d.DiscountStarting <= o.OrderDate 
                            AND d.DiscountExpiring >= o.OrderDate
                        ORDER BY d.DiscountValue DESC), 0)
                END
            ");

            context.Database.ExecuteSqlRaw(@"
                CREATE FUNCTION GetFinalPrice(@OrderId INT) 
                RETURNS MONEY
                AS BEGIN
                    DECLARE 
                        @MostProfitableDiscount FLOAT,
                        @DetailPrice MONEY,
                        @ServicesPrice MONEY

                    SET @MostProfitableDiscount = dbo.GetDiscountValue(@OrderId)

                    SET @DetailPrice = COALESCE((
                        SELECT SUM(od.DetailPrice * od.DetailCount) AS SumPrice
                        FROM OrderDetails AS od
                        WHERE od.OrderId = @OrderId), 0)

                    SET @ServicesPrice = COALESCE((
                        SELECT SUM(s.ServicePrice)
                        FROM OrderService AS os
                        INNER JOIN Services AS s ON s.ServiceId = os.ServicesServiceId
                        WHERE os.OrdersOrderId = @OrderId), 0)

                    RETURN @ServicesPrice * (1 - @MostProfitableDiscount) + @DetailPrice
                END
            ");

            context.Database.ExecuteSqlRaw(@"
                CREATE TRIGGER NewClientDiscount ON Clients
                FOR INSERT 
                AS BEGIN
	                INSERT INTO Discounts(DiscountStarting, DiscountExpiring, DiscountValue)
	                SELECT CURRENT_TIMESTAMP, DATEADD(WEEK, 1, CURRENT_TIMESTAMP), 0.1
	                FROM inserted
                    
                    INSERT INTO ClientDiscount(ClientsUserId, DiscountsDiscountId)
                    SELECT UserId, SCOPE_IDENTITY()
                    FROM inserted
                END
            ");
        }

        private static void SeedData(IServiceProvider services)
        {
            var context = services.GetRequiredService<ApiDbContext>();
            var userManager = services.GetRequiredService<UserManager<User>>();
            var roleManager = services.GetRequiredService<RoleManager<Role>>();

            roleManager.CreateAsync(new Role { Name = ClientRoleName }).GetAwaiter().GetResult();
            roleManager.CreateAsync(new Role { Name = EmployeeRoleName }).GetAwaiter().GetResult();
            roleManager.CreateAsync(new Role { Name = ManagerRoleName }).GetAwaiter().GetResult();

            var jobNames = File.ReadAllText(@"MockData\JobNames.csv").Split(',');
            var usersJson = File.ReadAllText(@"MockData\Users.json");
            var usersList = JsonConvert.DeserializeObject<List<User>>(usersJson);
            for (int i = 0; i < 10; i++)
            {
                userManager.CreateAsync(usersList[i], "P@ssw0rd").GetAwaiter().GetResult();
                userManager.AddToRoleAsync(usersList[i], ClientRoleName).GetAwaiter().GetResult();
                context.Add(new Client { UserId = usersList[i].Id });
            }
            for (int i = 10; i < 20; i++)
            {
                userManager.CreateAsync(usersList[i], "P@ssw0rd").GetAwaiter().GetResult();
                userManager.AddToRoleAsync(usersList[i], EmployeeRoleName).GetAwaiter().GetResult();
                context.Add(new Employee
                {
                    EmployeeJob = jobNames[i % jobNames.Length],
                    EmployeePass = $"pass{i}",
                    EmployeeSalary = i * 2000,
                    UserId = usersList[i].Id
                });
            }
            for (int i = 20; i < 30; i++)
            {
                userManager.CreateAsync(usersList[i], "P@ssw0rd").GetAwaiter().GetResult();
                userManager.AddToRoleAsync(usersList[i], ManagerRoleName).GetAwaiter().GetResult();
            }
            context.SaveChanges();

            var deviceTypesNames = File.ReadAllText(@"MockData\DeviceTypeNames.csv").Split(',');
            var deviceTypesList = deviceTypesNames
                .Select(deviceTypeName => new DeviceType { DeviceTypeName = deviceTypeName });
            context.AddRange(deviceTypesList);
            context.SaveChanges();

            var orderDetailStatusesNames = File.ReadAllText(@"MockData\OrderDetailStatusNames.csv").Split(',');
            var orderDetailStatusesList = orderDetailStatusesNames
                .Select(orderDetailStatusName => new OrderDetailStatus { OrderDetailStatusName = orderDetailStatusName });
            context.AddRange(orderDetailStatusesList);
            context.SaveChanges();

            var orderStatusesNames = File.ReadAllText(@"MockData\OrderStatusNames.csv").Split(',');
            var orderStatusesList = orderStatusesNames
                .Select(orderStatusName => new OrderStatus { OrderStatusName = orderStatusName });
            context.AddRange(orderStatusesList);
            context.SaveChanges();

            var servicesJson = File.ReadAllText(@"MockData\Services.json");
            var servicesList = JsonConvert.DeserializeObject<List<Service>>(servicesJson);
            context.AddRange(servicesList);
            context.SaveChanges();

            var ordersJson = File.ReadAllText(@"MockData\Orders.json");
            var ordersList = JsonConvert.DeserializeObject<List<Order>>(ordersJson);
            context.AddRange(ordersList);
            context.SaveChanges();

            foreach (var o in context.Orders.ToList())
            {
                foreach (var s in context.Services.Where(s => s.DeviceType == o.DeviceType).ToList())
                {
                    if (new Random().Next(2) == 0)
                    {
                        o.Services.Add(s);
                    }
                }

                foreach (var od in o.OrderDetails.ToList())
                {
                    od.Detail.DeviceTypes.Add(context.DeviceTypes.Single(dt => dt.DeviceTypeId == o.DeviceTypeId));
                }
            }
            context.SaveChanges();
        }
    }
}
