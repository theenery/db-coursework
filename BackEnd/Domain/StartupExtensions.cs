﻿using Domain.Extentions;
using Domain.Middleware;
using Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;

namespace Domain
{
    public static class StartupExtensions
    {
        public static WebApplicationBuilder ConfigureServices(this WebApplicationBuilder builder, string dbUserName)
        {
            builder.Configuration.AddSharedSettings();

            return builder
                .AddCors("CorsPolicy")
                .AddAutoMapperService()
                .AddDbContextService(dbUserName)
                .AddAuthService()
                .AddServiceDi()
                .AddRepositoryDi()
                .ConfigureControllers();
        }

        public static WebApplication Configure(this WebApplication app) {
            app.UseCors("CorsPolicy");

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.MapControllers();

            return app;
        }
    }
}
