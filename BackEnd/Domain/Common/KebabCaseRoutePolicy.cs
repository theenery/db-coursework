﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace Domain.Common
{
    public class KebabCaseRoutePolicy : IApplicationModelConvention
    {
        public void Apply(ApplicationModel application)
        {
            foreach (var controller in application.Controllers)
            {
                var controllerName = controller.ControllerName;
                controller.ControllerName = ToKebabCase(controllerName);

                foreach (var action in controller.Actions)
                {
                    var actionName = action.ActionName;
                    action.ActionName = ToKebabCase(actionName);
                }
            }
        }

        private string ToKebabCase(string input)
        {
            return string.Concat(input.Select((x, i) => i > 0 && char.IsUpper(x) ? "-" + x.ToString() : x.ToString())).ToLower();
        }
    }
}