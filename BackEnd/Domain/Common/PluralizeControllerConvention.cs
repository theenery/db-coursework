﻿using Humanizer;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace Domain.Common
{
    public class PluralizeControllerConvention : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            controller.ControllerName = controller.ControllerName.Pluralize();
        }
    }
}
