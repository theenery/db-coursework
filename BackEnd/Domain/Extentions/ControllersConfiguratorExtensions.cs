﻿using Domain.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.Extentions
{
    public static class ControllersConfiguratorExtensions
    {
        public static WebApplicationBuilder ConfigureControllers(this WebApplicationBuilder builder)
        {
            builder.Services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            builder.Services.AddControllers(configure =>
            {
                configure.Conventions.Add(new PluralizeControllerConvention());
                configure.Conventions.Add(new KebabCaseRoutePolicy());
            });
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            return builder;
        }
    }
}
