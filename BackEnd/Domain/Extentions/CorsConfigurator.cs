﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.Extentions
{
    public static class CorsConfigurator
    {
        public static WebApplicationBuilder AddCors(this WebApplicationBuilder builder, string name)
        {
            builder.Services.AddCors(options => {
                options.AddPolicy(name,
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });

            return builder;
        }
    }
}
