﻿using Domain.Services.Implementations;
using Domain.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Extensions
{
    public static class ServiceDiExtentions
    {
        public static WebApplicationBuilder AddServiceDi(this WebApplicationBuilder builder)
        {
            builder.Services
                .AddTransient<IAccountService, AccountService>()
                .AddTransient<IDetailService, DetailService>()
                .AddTransient<IOrderService, OrderService>()
                .AddTransient<IOrderDetailService, OrderDetailService>();

            return builder;
        }
    }
}
