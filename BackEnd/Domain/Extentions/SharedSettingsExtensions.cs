﻿using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace Domain.Extentions
{
    public static class SharedSettingsExtensions
    {
        public static IConfigurationBuilder AddSharedSettings(this IConfigurationBuilder configuration)
        {
            var relativePath = @"../sharedSettings.json";
            var absolutePath = Path.GetFullPath(relativePath);

            configuration.AddJsonFile(absolutePath, true, true);

            return configuration;
        }
    }
}
