﻿using Infrastructure.Dto;
using Infrastructure.Models;

namespace Domain.Services.Interfaces
{
    public interface IAccountService
    {
        Task<User> FindByEmailAsync(string email);
        Task<string> GenerateTokenStringAsync(User user);
        Task<string> GetRoleAsync(User user);
        Task<bool> LoginUser(LoginUserDto user);
        Task<bool> RegisterUser(RegisterUserDto user);
    }
}