﻿using Infrastructure.Models;
using System.Linq.Expressions;

namespace Domain.Services.Interfaces
{
    public interface IOrderService
    {
        public Task<IEnumerable<Order>> GetPagedAsync(Expression<Func<Order, bool>> filter, int count, int page, string orderBy, bool isDescending);
    }
}
