﻿using Infrastructure.Models;
using System.Linq.Expressions;

namespace Domain.Services.Interfaces
{
    public interface IOrderDetailService
    {
        public Task<IEnumerable<OrderDetail>> GetPagedAsync(Expression<Func<OrderDetail, bool>> filter, int count, int page, string orderBy, bool isDescending);
    }
}
