﻿using Infrastructure.Models;
using System.Linq.Expressions;

namespace Domain.Services.Interfaces
{
    public interface IDetailService
    {
        public Task<IEnumerable<Detail>> GetPagedAsync(Expression<Func<Detail, bool>> filter, int count, int page, string orderBy, bool isDescending);
    }
}
