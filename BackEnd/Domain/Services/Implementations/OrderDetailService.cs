﻿using Domain.Services.Interfaces;
using Infrastructure.Extensions;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using System.Linq.Expressions;

namespace Domain.Services.Implementations
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IOrderDetailRepository _orderDetailRepository;

        public OrderDetailService(IOrderDetailRepository orderDetailRepository)
        {
            _orderDetailRepository = orderDetailRepository;
        }

        public async Task<IEnumerable<OrderDetail>> GetPagedAsync(Expression<Func<OrderDetail, bool>>? filter, int count, int page, string orderBy, bool isDescending)
        {
            var orderDetails = _orderDetailRepository.GetAll(filter);

            switch (orderBy)
            {
                case "count":
                    {
                        orderDetails = orderDetails.OrderBy(od => od.DetailCount);
                        break;
                    }
                case "price":
                    {
                        orderDetails = orderDetails.OrderBy(od => od.DetailPrice);
                        break;
                    }
                case "name":
                    {
                        orderDetails = orderDetails.OrderBy(od => od.Detail.DetailName);
                        break;
                    }
                default:
                    {
                        orderDetails = orderDetails.OrderBy(od => od.Detail.DetailName);
                        break;
                    };
            }

            if (isDescending)
            {
                orderDetails = orderDetails.Reverse();
            }

            return await orderDetails.GetPagedAsync(count, page);
        }
    }
}
