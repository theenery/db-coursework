﻿using Domain.Services.Interfaces;
using Infrastructure.Extensions;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using System.Linq.Expressions;

namespace Domain.Services.Implementations
{
    public class DetailService : IDetailService
    {
        private readonly IDetailRepository _detailRepository;

        public DetailService(IDetailRepository detailRepository)
        {
            _detailRepository = detailRepository;
        }

        public async Task<IEnumerable<Detail>> GetPagedAsync(Expression<Func<Detail, bool>> filter, int count, int page, string orderBy, bool isDescending)
        {
            var detail = _detailRepository.GetAll(filter);

            switch (orderBy)
            {
                case "id":
                    {
                        detail = detail.OrderBy(d => d.DetailId);
                        break;
                    }
                case "name":
                    {
                        detail = detail.OrderBy(d => d.DetailName);
                        break;
                    }
                case "count":
                    {
                        detail = detail.OrderBy(d => d.DetailCount);
                        break;
                    }
                case "price":
                    {
                        detail = detail.OrderBy(d => d.DetailPrice);
                        break;
                    }
                default:
                    {
                        detail = detail.OrderBy(d => d.DetailName);
                        break;
                    };
            }

            if (isDescending)
            {
                detail = detail.Reverse();
            }

            return await detail.GetPagedAsync(count, page);
        }
    }
}
