﻿using Domain.Services.Interfaces;
using Infrastructure.Extensions;
using Infrastructure.Models;
using Infrastructure.Repository.Interfaces;
using System.Linq.Expressions;

namespace Domain.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<IEnumerable<Order>> GetPagedAsync(Expression<Func<Order, bool>>? filter, int count, int page, string orderBy, bool isDescending)
        {
            var orders = _orderRepository.GetAll(filter);

            switch (orderBy)
            {
                case "date":
                    {
                        orders = orders.OrderBy(o => o.OrderDate);
                        break;
                    }
                case "status":
                    {
                        orders = orders.OrderBy(o => o.OrderStatusId);
                        break;
                    }
                default:
                    {
                        orders = orders.OrderBy(o => o.OrderDate);
                        break;
                    };
            }

            if (isDescending)
            {
                orders = orders.Reverse();
            }

            return await orders.GetPagedAsync(count, page);
        }
    }
}
