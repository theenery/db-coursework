const baseApiUrl = 'https://localhost:';

const mainApiPort = '7098';
const clientApiPort = '7272';
const employeeApiPort = '7233';
const managerApiPort = '7130';

export const environment = {
  mainApi: baseApiUrl + mainApiPort + '/main-api',
  clientApi: baseApiUrl + clientApiPort + '/client-api',
  employeeApi: baseApiUrl + employeeApiPort + '/employee-api',
  managerApi: baseApiUrl + managerApiPort + '/manager-api'
};

