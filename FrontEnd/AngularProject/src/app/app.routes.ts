import { Routes } from '@angular/router';
import { HomePageComponent } from './features/components/client/home-page/home-page.component';
import { OrderPageComponent as copc } from './features/components/client/order-page/order-page.component';
import { OrdersPageComponent as cospc } from './features/components/client/orders-page/orders-page.component';
import { DetailsPageComponent as edpc } from './features/components/employee/details-page/details-page.component';
import { OrderPageComponent as eopc } from './features/components/employee/order-page/order-page.component';
import { OrdersPageComponent as eospc } from './features/components/employee/orders-page/orders-page.component';
import { DetailsPageComponent as mdpc } from './features/components/manager/details-page/details-page.component';
import { OrderPageComponent as mopc } from './features/components/manager/order-page/order-page.component';
import { OrdersPageComponent as mospc } from './features/components/manager/orders-page/orders-page.component';
import { SignInComponent } from './features/components/sign-in/sign-in.component';
import { SignUpComponent } from './features/components/sign-up/sign-up.component';
import { AnalyticsComponent } from './features/components/manager/analytics-page/analytics-page.component';
import { OrderDetailsPageComponent } from './features/components/manager/order-details-page/order-details-page.component';

export const routes: Routes = [
  //{
  //  path: 'services', loadComponent: () => 
  //    import('./features/components/service-list/service-list.component')
  //      .then(mod => mod.ServiceListComponent),
  //  canActivate: [authGuard]
  //},
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'client-orders', component: cospc },
  { path: 'client-orders/:id', component: copc },
  { path: 'employee-orders', component: eospc },
  { path: 'employee-orders/:id', component: eopc },
  { path: 'employee-details', component: edpc },
  { path: 'manager-orders', component: mospc },
  { path: 'manager-orders/:id', component: mopc },
  { path: 'manager-details', component: mdpc },
  { path: 'manager-analytics', component: AnalyticsComponent },
  { path: 'manager-order-details', component: OrderDetailsPageComponent },
  { path: '', component: HomePageComponent }
];
