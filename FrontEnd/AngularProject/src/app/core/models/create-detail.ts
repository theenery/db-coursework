import { DeviceType } from "./device-type";

export interface DetailWithDeviceTypes {
  detailId: number,
  detailName: string,
  detailCount: number,
  detailPrice: number,
  deviceTypes: DeviceType[]
}
