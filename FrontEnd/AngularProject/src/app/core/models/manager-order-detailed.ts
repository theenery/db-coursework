import { Device } from "./device"
import { DeviceType } from "./device-type"
import { Employee } from "./employee"
import { OrderDetail } from "./order-detail"
import { OrderStatus } from "./order-status"
import { Service } from "./service"
import { UserWithContacts } from "./user-with-contacts"

export interface ManagerOrderDetailed {
  orderId: number,
  client: {
    user: UserWithContacts
  },
  employee?: Employee,
  device: Device,
  deviceType: DeviceType,
  orderDate: Date,
  orderDescription: string,
  orderStatus: OrderStatus,
  services: Service[],
  orderDetails: OrderDetail[]
}
