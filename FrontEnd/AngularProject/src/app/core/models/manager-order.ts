import { DeviceType } from "./device-type"
import { OrderStatus } from "./order-status"

export interface ManagerOrder {
  orderId: number,
  deviceType: DeviceType,
  orderDate: Date,
  orderStatus: OrderStatus,
  orderPrice: number,
  orderDescription: string
}
