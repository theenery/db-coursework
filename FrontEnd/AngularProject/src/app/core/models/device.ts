export interface Device {
  deviceId: number,
  deviceTypeId: number,
  deviceMaker: string,
  deviceModel: string
}
