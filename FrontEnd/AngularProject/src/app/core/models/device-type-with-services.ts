import { Service } from "./service";

export interface DeviceTypeWithServices {
  deviceTypeId: number,
  deviceTypeName: string,
  services: Service[]
}
