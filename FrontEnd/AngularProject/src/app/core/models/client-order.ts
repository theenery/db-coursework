import { DeviceType } from "./device-type"
import { OrderStatus } from "./order-status"

export interface ClientOrder {
  orderId: number,
  deviceType: DeviceType,
  orderDate: Date,
  orderStatus: OrderStatus,
  orderPrice: number,
}
