export interface SignInResult {
  id: number,
  firstName: string,
  lastName: string,
  role: string,
  token: string
}
