export interface Discount {
  discountId: number,
  discountStarting: Date,
  discountExpiring: Date,
  discountValue: number
}
