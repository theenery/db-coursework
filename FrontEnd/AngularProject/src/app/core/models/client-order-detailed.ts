import { Device } from "./device"
import { DeviceType } from "./device-type"
import { Discount } from "./discount"
import { OrderDetail } from "./order-detail"
import { OrderStatus } from "./order-status"
import { Service } from "./service"
import { User } from "./user"

export interface ClientOrderDetailed {
  orderId: number,
  employee?: { user: User },
  device?: Device,
  deviceType: DeviceType,
  orderDiscountValue: number,
  orderDate: Date,
  orderDescription: string,
  orderStatus: OrderStatus,
  services: Service[],
  orderDetails: OrderDetail[]
}
