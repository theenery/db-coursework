export interface CreateOrder {
  deviceTypeId: number,
  orderDescription: string,
}
