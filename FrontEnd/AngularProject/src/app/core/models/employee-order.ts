import { Device } from "./device";
import { DeviceType } from "./device-type";
import { OrderStatus } from "./order-status";

export interface EmployeeOrder {
  orderId: number,
  device?: Device,
  deviceType: DeviceType,
  orderDate: Date,
  orderDescription: string,
  orderStatus: OrderStatus,
}
