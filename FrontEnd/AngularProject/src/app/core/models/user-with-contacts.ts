export interface UserWithContacts {
  email: string,
  phoneNumber?: string,
  firstName: string,
  lastName: string
}
