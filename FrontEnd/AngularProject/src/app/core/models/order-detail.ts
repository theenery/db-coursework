import { OrderDetailStatus } from "./order-detail-status";

export interface OrderDetail {
  orderId: number,
  detailId: number,
  detailName: string,
  detailPrice: number,
  detailCount: number,
  orderDetailStatus: OrderDetailStatus
}
