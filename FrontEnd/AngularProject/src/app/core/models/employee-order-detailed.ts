import { Device } from "./device";
import { DeviceType } from "./device-type";
import { OrderDetail } from "./order-detail";
import { OrderStatus } from "./order-status";
import { Service } from "./service";
import { UserWithContacts } from "./user-with-contacts";

export interface EmployeeOrderDetailed {
  orderId: number,
  client: { user: UserWithContacts },
  device: Device,
  deviceType: DeviceType,
  orderDate: Date,
  orderDescription: string,
  orderStatus: OrderStatus,
  services: Service[],
  orderDetails: OrderDetail[]
}
