export interface Detail {
  detailId: number,
  detailName: string,
  detailCount: number,
  detailPrice: number,
}
