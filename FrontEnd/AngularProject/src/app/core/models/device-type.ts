export interface DeviceType {
  deviceTypeId: number,
  deviceTypeName: string,
}
