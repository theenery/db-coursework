import { User } from "./user";

export interface Employee {
  userId: number,
  employeeJob: string,
  user: User
}
