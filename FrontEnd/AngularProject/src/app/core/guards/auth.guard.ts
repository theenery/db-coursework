import { CanActivateFn } from '@angular/router';
import { AccountService } from '../services/main/account.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  return inject(AccountService).isSignedIn();
};
