import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { Detail } from '../../models/detail';
import { DetailWithDeviceTypes } from '../../models/create-detail';

@Injectable({
  providedIn: 'root'
})
export class DetailService {
  constructor(private http: HttpClient) { }

  create(detail: DetailWithDeviceTypes) {
    return this.http.post<DetailWithDeviceTypes>(`${environment.employeeApi}/details`, detail);
  }

  getCount(deviceType: number, name: string) {
    let params = new HttpParams();
    params = params.append('deviceType', deviceType);
    params = params.append('name', name);

    return this.http.get<number>(`${environment.employeeApi}/details/count`, { params: params });
  }

  getDetail(id: number) {
    return this.http.get<DetailWithDeviceTypes>(`${environment.employeeApi}/details/${id}`);
  }

  getPaged(deviceType: number, name: string, count: number, page: number, orderBy: string, isDescending: boolean) {
    let params = new HttpParams();
    params = params.append('deviceType', deviceType);
    params = params.append('name', name);
    params = params.append('count', count);
    params = params.append('page', page);
    params = params.append('orderBy', orderBy);
    params = params.append('isDescending', isDescending);

    return this.http.get<Detail[]>(`${environment.employeeApi}/details`, { params: params });
  }

  update(detail: DetailWithDeviceTypes) {
    return this.http.put<DetailWithDeviceTypes>(`${environment.employeeApi}/details/${detail.detailId}`, detail);
  }
}
