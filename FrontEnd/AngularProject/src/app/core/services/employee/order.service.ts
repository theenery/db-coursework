import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { EmployeeOrder } from '../../models/employee-order';
import { EmployeeOrderDetailed } from '../../models/employee-order-detailed';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient) { }

  getCount() {
    return this.http.get<number>(`${environment.employeeApi}/employees/${localStorage.getItem('id')}/orders/count`);
  }

  getOrder(id: number) {
    return this.http.get<EmployeeOrderDetailed>(`${environment.employeeApi}/employees/${localStorage.getItem('id')}/orders/${id}`);
  }

  getPaged(count: number, page: number, orderBy: string, isDescending: boolean) {
    let params = new HttpParams();
    params = params.append('count', count);
    params = params.append('page', page);
    params = params.append('orderBy', orderBy);
    params = params.append('isDescending', isDescending);
    
    return this.http.get<EmployeeOrder[]>(`${environment.employeeApi}/employees/${localStorage.getItem('id')}/orders`, { params: params });
  }

  update(order: EmployeeOrder) {
    return this.http.put<EmployeeOrderDetailed>(`${environment.employeeApi}/employees/${localStorage.getItem('id')}/orders/${order.orderId}`, order);
  }
}
