import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { ManagerOrder } from '../../models/manager-order';
import { ManagerOrderDetailed } from '../../models/manager-order-detailed';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient) { }

  getCount(orderStatusId: number) {
    let params = new HttpParams();
    params = params.append('orderStatusId', orderStatusId);

    return this.http.get<number>(`${environment.managerApi}/orders/count`, { params: params });
  }

  getOrder(id: number) {
    return this.http.get<ManagerOrderDetailed>(`${environment.managerApi}/orders/${id}`);
  }

  getPaged(count: number, page: number, orderBy: string, isDescending: boolean, orderStatusId: number) {
    let params = new HttpParams();
    params = params.append('count', count);
    params = params.append('page', page);
    params = params.append('orderBy', orderBy);
    params = params.append('isDescending', isDescending);
    params = params.append('orderStatusId', orderStatusId);

    return this.http.get<ManagerOrder[]>(`${environment.managerApi}/orders`, { params: params });
  }

  update(order: ManagerOrderDetailed) {
    return this.http.put<ManagerOrderDetailed>(`${environment.managerApi}/orders/${order.orderId}`, order);
  }
}
