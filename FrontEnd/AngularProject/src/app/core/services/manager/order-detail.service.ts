import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { OrderDetail } from '../../models/order-detail';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailService {
  constructor(private http: HttpClient) { }

  getCount(orderDetailStatus: number, name: string) {
    let params = new HttpParams();
    params = params.append('orderDetailStatus', orderDetailStatus);
    params = params.append('name', name);

    return this.http.get<number>(`${environment.managerApi}/order-details/count`, { params: params });
  }

  getPaged(orderDetailStatus: number, name: string, count: number, page: number, orderBy: string, isDescending: boolean) {
    let params = new HttpParams();
    params = params.append('orderDetailStatus', orderDetailStatus);
    params = params.append('name', name);
    params = params.append('count', count);
    params = params.append('page', page);
    params = params.append('orderBy', orderBy);
    params = params.append('isDescending', isDescending);

    return this.http.get<OrderDetail[]>(`${environment.managerApi}/order-details`, { params: params });
  }

  update(detail: OrderDetail) {
    return this.http.put<OrderDetail>(`${environment.managerApi}/order-details/${detail.detailId}`, detail);
  }
}
