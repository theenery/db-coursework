import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  constructor(private http: HttpClient) { }

  getData(fromDate: Date, toDate: Date) {
    let params = new HttpParams();
    params = params.append('fromDate', fromDate.toISOString());
    params = params.append('toDate', toDate.toISOString());

    return this.http.get(`${environment.managerApi}/analytics/export-data`, { params: params });
  }

  getDateRange() {
    return this.http.get<{ min: Date, max: Date }>(`${environment.managerApi}/analytics/date-range`);
  }

  getIncomes(type: string, fromDate: Date, toDate: Date) {
    let params = new HttpParams();
    params = params.append('type', type);
    params = params.append('fromDate', fromDate.toISOString());
    params = params.append('toDate', toDate.toISOString());

    return this.http.get<{ key: number, value: number }[]>(`${environment.managerApi}/analytics`, { params: params });
  }
}
