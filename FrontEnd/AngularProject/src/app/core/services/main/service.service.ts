import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { DeviceTypeWithServices } from '../../models/device-type-with-services';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<DeviceTypeWithServices[]>(`${environment.mainApi}/services`);
  }
}
