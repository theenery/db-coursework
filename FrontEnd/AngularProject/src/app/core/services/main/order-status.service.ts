import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { OrderStatus } from '../../models/order-status';

@Injectable({
  providedIn: 'root'
})
export class OrderStatusService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<OrderStatus[]>(`${environment.mainApi}/order-statuses`);
  }
}
