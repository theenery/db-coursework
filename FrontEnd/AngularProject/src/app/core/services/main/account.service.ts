import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SignInModel } from '../../models/sign-in';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SignUpModel } from '../../models/sign-up';
import { SignInResult } from '../../models/sign-in-result';
import { environment } from '../../../../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  jwtHelper = new JwtHelperService();

  constructor(private http: HttpClient) { }

  isSignedIn() {
    return !this.jwtHelper.isTokenExpired(localStorage.getItem('jwt_token'));
  }

  signIn(credentials: SignInModel) {
    return this.http.post<SignInResult>(`${environment.mainApi}/accounts`, credentials)
      .pipe(
        map(result => {
          if (result && result.token) {
            localStorage.setItem('id', result.id.toString());
            localStorage.setItem('jwt_token', result.token);
            localStorage.setItem('firstname', result.firstName);
            localStorage.setItem('lastname', result.lastName);
            localStorage.setItem('role', result.role);
          }
          return result;
        })
      );
  }

  signOut() {
    localStorage.removeItem('id');
    localStorage.removeItem('jwt_token');
    localStorage.removeItem('firstname');
    localStorage.removeItem('lastname');
    localStorage.removeItem('role');
  }

  signUp(credentials: SignUpModel) {
    return this.http.put(`${environment.mainApi}/accounts`, credentials);
  }
}
