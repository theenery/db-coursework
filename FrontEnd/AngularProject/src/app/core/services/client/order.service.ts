import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { CreateOrder } from '../../models/create-order';
import { ClientOrder } from '../../models/client-order';
import { ClientOrderDetailed } from '../../models/client-order-detailed';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient) { }

  getCount() {
    return this.http.get<number>(`${environment.clientApi}/clients/${localStorage.getItem('id')}/orders/count`);
  }

  getOrder(id: number) {
    return this.http.get<ClientOrderDetailed>(`${environment.clientApi}/clients/${localStorage.getItem('id')}/orders/${id}`);
  }

  getPaged(count: number, page: number, orderBy: string, isDescending: boolean) {
    let params = new HttpParams();
    params = params.append('count', count);
    params = params.append('page', page);
    params = params.append('orderBy', orderBy);
    params = params.append('isDescending', isDescending);

    return this.http.get<ClientOrder[]>(`${environment.clientApi}/clients/${localStorage.getItem('id')}/orders`, { params: params });
  }

  create(order: CreateOrder) {
    return this.http.post<CreateOrder>(`${environment.clientApi}/clients/${localStorage.getItem('id')}/orders`, order);
  }
}
