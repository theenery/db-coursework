import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ActivatedRoute } from '@angular/router';
import { EmployeeOrderDetailed } from '../../../../core/models/employee-order-detailed';
import { OrderService } from '../../../../core/services/employee/order.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';
import { Service } from '../../../../core/models/service';
import { ServiceService } from '../../../../core/services/main/service.service';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { OrderDetail } from '../../../../core/models/order-detail';
import { OrderDetailDialogComponent } from '../../../../shared/components/order-detail-dialog/order-detail-dialog.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

@Component({
  selector: 'app-employee-order-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    FormsModule,
    HeaderComponent,
    MatButtonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatSortModule,
    MatTableModule,
  ],
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent {
  order!: EmployeeOrderDetailed;
  services: Service[] = [];
  displayedColumns = ['detailId', 'detailName', 'detailCount', 'detailPrice', 'orderDetailStatus.orderDetailStatusName'];

  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService,
    private serviceService: ServiceService,
    public dialog: MatDialog
  ) {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id == null) {
        return;
      }
      this.orderService.getOrder(Number.parseInt(id)).subscribe(result => {
        this.order = result;
        if (this.order.device == null) {
          this.order.device = { deviceId: 0, deviceMaker: '', deviceModel: '', deviceTypeId: this.order.deviceType.deviceTypeId };
        }

        this.serviceService.getAll().subscribe(result => {
          this.services = result
            .filter(dt => dt.deviceTypeId == this.order.deviceType.deviceTypeId)[0]
            .services;
        });
      });
    });
  }

  addService(service: Service) {
    if (service && !this.order.services.map(s => s.serviceId).includes(service.serviceId)) {
      this.order.services.push(service);
    }
  }

  complete() {
    this.order.orderStatus.orderStatusId = 4;
  }

  removeService(service: Service) {
    const index = this.order.services.indexOf(service);
    if (index !== -1) {
      this.order.services.splice(index, 1);
    }
  }

  save() {
    this.orderService.update(this.order).subscribe(result => alert('Saved'));
  }

  addDetail() {
    const dialogRef = this.dialog.open(OrderDetailDialogComponent, {
      data: {
        action: 'Add',
        deviceType: this.order.deviceType.deviceTypeId,
        detail: { detailId: 0, detailCount: 0, detailPrice: 0, detailName: '', orderDetailStatus: { orderDetailStatusId: 1, orderDetailStatusName: 'Under consideration' } }
      },
    });

    dialogRef.afterClosed().subscribe((result: OrderDetail) => {
      if (result != null) {
        this.order.orderDetails = [ ...this.order.orderDetails, result];

      }
    });
  }

  editDetail(detail: OrderDetail) {
    const dialogRef = this.dialog.open(OrderDetailDialogComponent, {
      data: {
        detail: detail,
        deviceType: this.order.deviceType.deviceTypeId,
        action: 'Edit',
      },
    });

    dialogRef.afterClosed().subscribe((result: OrderDetail) => {
      if (result != null) {
        const index = this.order.orderDetails.findIndex(d => d.detailId == result.detailId);
        if (index !== -1) {
          this.order.orderDetails.splice(index, 1, result);
        }
      }
    });
  }
}
