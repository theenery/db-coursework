import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge } from 'rxjs';
import { DetailWithDeviceTypes } from '../../../../core/models/create-detail';
import { Detail } from '../../../../core/models/detail';
import { DeviceType } from '../../../../core/models/device-type';
import { DetailService } from '../../../../core/services/employee/detail.service';
import { ServiceService } from '../../../../core/services/main/service.service';
import { DetailDialogComponent } from '../../../../shared/components/detail-dialog/detail-dialog.component';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';


@Component({
  selector: 'app-employee-details-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    FormsModule,
    HeaderComponent,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
  ],
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.scss']
})
export class DetailsPageComponent implements OnInit, AfterViewInit {
  displayedColumns = ['detailId', 'detailName', 'detailCount', 'detailPrice'];
  deviceType = 1;
  name = '';
  deviceTypes: DeviceType[] = [];
  isDescending = false;
  details: Detail[] = [];
  detailCount = 0;
  pageIndex = 0;
  pageSize = 8;
  sortingOption = 'name';

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private detailService: DetailService,
    private serviceService: ServiceService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.serviceService.getAll().subscribe(result => {
      this.deviceTypes = result;

      this.route.queryParams
        .subscribe(params => {
          this.deviceType = Number.parseInt(params['deviceType'] ?? this.deviceTypes[0].deviceTypeId.toString());
          this.name = params['name'] ?? '';
          this.isDescending = params['isDescending'] ?? false;
          this.pageIndex = params['pageIndex'] ?? 0;
          this.pageSize = params['pageSize'] ?? 8;
          this.sortingOption = params['sortingOption'] ?? 'name';

          this.detailService.getCount(this.deviceType, this.name).subscribe(result => {
            this.detailCount = result;
            this.fetchDetails();
          });
        });
    });
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .subscribe(_ => this.update());
  }

  applyFilter(e: KeyboardEvent) {
    this.name = (e.target as HTMLInputElement).value;
    this.paginator.pageIndex = 0;
    this.update();
  }

  setDeviceType(e: MatSelectChange) {
    this.update();
  }

  fetchDetails() {
    this.detailService
      .getPaged(
        this.deviceType,
        this.name,
        this.pageSize,
        this.pageIndex,
        this.sortingOption,
        this.isDescending)
      .subscribe(result => {
        this.details = result;
      });
  }

  update() {
    this.router.navigate(
      ['employee-details'],
      {
        queryParams: {
          deviceType: this.deviceType,
          name: this.name,
          isDescending: this.sort.direction == 'desc',
          pageIndex: this.paginator.pageIndex,
          pageSize: this.paginator.pageSize,
          sortingOption: this.sort.active.slice('detail'.length).toLowerCase()
        }
      });
  }

  openAddDialog(): void {
    const dialogRef = this.dialog.open(DetailDialogComponent, {
      data: {
        detail: { detailId: 0, detailCount: 0, detailPrice: 0, detailName: '', deviceTypes: [] },
        action: 'Add',
        deviceTypes: this.deviceTypes
      },
    });

    dialogRef.afterClosed().subscribe((result: DetailWithDeviceTypes) => {
      if (result != null) { 
        this.detailService.create(result).subscribe(result => { console.log('ok') });
      }
    });
  }

  openEditDialog(detail: Detail): void {
    this.detailService.getDetail(detail.detailId).subscribe(result => {
      const dialogRef = this.dialog.open(DetailDialogComponent, {
        data: { detail: result, action: 'Edit', deviceTypes: this.deviceTypes },
      }); 

      dialogRef.afterClosed().subscribe((result: DetailWithDeviceTypes) => {
        if (result != null) {
          this.detailService.update(result).subscribe(result => { console.log('ok'); });
        }
      });
    });
  }
}
