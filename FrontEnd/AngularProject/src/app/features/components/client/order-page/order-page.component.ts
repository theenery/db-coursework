import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientOrderDetailed } from '../../../../core/models/client-order-detailed';
import { OrderService } from '../../../../core/services/client/order.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';

@Component({
  selector: 'app-client-order-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    HeaderComponent
  ],
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent {
  order!: ClientOrderDetailed;

  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService
  ) {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id == null) {
        return;
      }
      this.orderService.getOrder(Number.parseInt(id)).subscribe(result => {
        this.order = result;
      });
    });
  }

  calcPrice() {
    return this.order.services
      .reduce((price, service) => price + service.servicePrice, 0)
      * (1 - this.order.orderDiscountValue)
      + this.order.orderDetails
        .reduce((price, detail) => price + detail.detailPrice * detail.detailCount, 0);
  }
}
