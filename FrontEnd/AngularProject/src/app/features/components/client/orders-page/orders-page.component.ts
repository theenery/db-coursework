import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ClientOrder } from '../../../../core/models/client-order';
import { OrderService } from '../../../../core/services/client/order.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';
import { PaginatorComponent, PaginatorEvent } from '../../../../shared/components/paginator/paginator.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-orders-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    HeaderComponent,
    PaginatorComponent
  ],
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnInit {
  isDescending = false;
  orders: ClientOrder[] = [];
  orderCount = 0;
  pageIndex = 0;
  pageSize = 8;
  sortingOption = 'date';
  sortingOptions = ['date', 'status'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.isDescending = params['isDescending'] ?? false;
        this.pageIndex = params['pageIndex'] ?? 0;
        this.pageSize = params['pageSize'] ?? 8;
        this.sortingOption = params['sortingOption'] ?? 'date';

        this.orderService.getCount().subscribe(result => {
          this.orderCount = result;
          this.fetchOrders();
        });
      });
  }

  fetchOrders() {
    this.orderService
      .getPaged(
        this.pageSize,
        this.pageIndex,
        this.sortingOption,
        this.isDescending)
      .subscribe(result => {
        this.orders = result;
      });
  }

  goToOrder(id: number) {
    this.router.navigate(['client-orders', id]);
  }

  update(e: PaginatorEvent) {
    this.router.navigate(
      ['client-orders'],
      {
        queryParams: { ...e }
      });
  }
}
