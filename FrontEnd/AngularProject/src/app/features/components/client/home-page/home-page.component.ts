import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { DeviceTypeWithServices } from '../../../../core/models/device-type-with-services';
import { AccountService } from '../../../../core/services/main/account.service';
import { ServiceService } from '../../../../core/services/main/service.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { ReactiveFormsModule, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { OrderService } from '../../../../core/services/client/order.service';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [CommonModule, HeaderComponent, MatSelectModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, ReactiveFormsModule],
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  isSignIn = this.accountService.isSignedIn(); 
  aggregatedServices: DeviceTypeWithServices[] = [];
  createOrderForm: FormGroup;

  constructor(
    private accountService: AccountService,
    private serviceService: ServiceService,
    private orderService: OrderService,
    private formBuilder: NonNullableFormBuilder,
  ) {
    this.createOrderForm = formBuilder.group({
      deviceTypeId: ['', [Validators.required]],
      orderDescription: ['', [Validators.required]],
    })
  }

  ngOnInit() {
    this.serviceService.getAll().subscribe(result => {
      this.aggregatedServices = result;
    });
  }

  sendOrder() {
    this.orderService
      .create(this.createOrderForm.value)
      .subscribe({
        next: () => {
          alert('Your order has been accepted. We will contact you later.');
        },
        error: () => {
          alert('Failed');
        }
      })
  }
}
