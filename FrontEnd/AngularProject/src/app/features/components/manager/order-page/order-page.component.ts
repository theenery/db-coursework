import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../../../core/services/manager/order.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';
import { Service } from '../../../../core/models/service';
import { ServiceService } from '../../../../core/services/main/service.service';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { ManagerOrderDetailed } from '../../../../core/models/manager-order-detailed';
import { Employee } from '../../../../core/models/employee';
import { EmployeeService } from '../../../../core/services/manager/employee.service';

@Component({
  selector: 'app-manager-order-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    FormsModule,
    HeaderComponent,
    MatButtonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule
  ],
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent {
  employees: Employee[] = [];
  order!: ManagerOrderDetailed;
  services: Service[] = [];

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private orderService: OrderService,
    private serviceService: ServiceService
  ) {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id == null) {
        return;
      }
      this.orderService.getOrder(Number.parseInt(id)).subscribe(result => {
        this.order = result;
        if (this.order.employee == null) {
          this.order.employee = {
            userId: 0, user: {firstName: '', lastName: ''}, employeeJob: '' };
        }
        if (this.order.device == null) {
          this.order.device = { deviceId: 0, deviceMaker: '', deviceModel: '', deviceTypeId: 0 };
        }

        this.serviceService.getAll().subscribe(result => {
          this.services = result
            .filter(dt => dt.deviceTypeId == this.order.deviceType.deviceTypeId)[0]
            .services;
        });

        this.employeeService.getAll().subscribe(result => {
          this.employees = result;
        })
      });
    });
  }

  addService(service: Service) {
    if (service && !this.order.services.map(s => s.serviceId).includes(service.serviceId)) {
      this.order.services.push(service);
    }
  }

  removeService(service: Service) {
    const index = this.order.services.indexOf(service);
    if (index !== -1) {
      this.order.services.splice(index, 1);
    }
  }

  save() {
    this.orderService.update(this.order).subscribe(result => result);
  }
}
