import { CommonModule } from "@angular/common";
import { Component, ViewChild } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { HeaderComponent } from "../../../../shared/components/header/header.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatPaginatorModule, PageEvent } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { CanvasJSAngularChartsModule, CanvasJSChart } from '@canvasjs/angular-charts';
import { AnalyticsService } from "../../../../core/services/manager/analytics.service";
import { combineLatest, from, tap } from "rxjs";

@Component({
  selector: 'app-manager-analytics-page',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    HeaderComponent,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSelectModule,
    CanvasJSAngularChartsModule
  ],
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.scss']
})
export class AnalyticsComponent {
  yearsCount = 0;
  year = 0;
  pageIndex = 0;

  constructor(private analyticsService: AnalyticsService) {
    this.analyticsService.getDateRange().subscribe(result => {
      this.yearsCount = new Date(result.max).getFullYear() - new Date(result.min).getFullYear() + 1;
      this.year = new Date(result.max).getFullYear();
      this.pageIndex = this.yearsCount - 1;
      this.update(null);
    });
  }

  update(e: PageEvent | null) {
    this.pageIndex = e?.pageIndex ?? this.pageIndex;
    const year = this.year - this.yearsCount + this.pageIndex + 1
    const fromDate = new Date(year, 0, 2);
    const toDate = new Date(year, 11, 32);
    this.chartComponent.options.title.text = year;
    
    combineLatest(
      [
        this.analyticsService.getIncomes('services', fromDate, toDate),
        this.analyticsService.getIncomes('orderDetails', fromDate, toDate)
      ]).pipe(
        tap(([responce0, responce1]) => {
          const data0 = responce0.map(kv => { return { x: new Date(0, kv.key - 1), y: kv.value } });
          data0.sort((a, b) => a.x < b.x ? -1 : a.x > b.x ? 1 : 0);
          this.chartComponent.options.data[0].dataPoints = data0;

          const data1 = responce1.map(kv => { return { x: new Date(0, kv.key - 1), y: kv.value } });
          data1.sort((a, b) => a.x < b.x ? -1 : a.x > b.x ? 1 : 0);
          this.chartComponent.options.data[1].dataPoints = data1;

          const data2 = data0.map((xy, index) => { return { x: xy.x, y: xy.y + data1[index].y } });
          this.chartComponent.options.data[2].dataPoints = data2;

          this.chartComponent.chart.render();
        })).subscribe({
          error: error => {
            console.error('Error:', error);
          }
        });
  }

  @ViewChild('chart') chartComponent!: CanvasJSChart;

  chartOptions = {
    animationEnabled: true,
    theme: "light2",
    title: {
      text: "Analytics"
    },
    axisX: {
      valueFormatString: "MMM",
      interval: 1,
      intervalType: 'month'
    },
    axisY: {
      title: ""
    },
    toolTip: {
      shared: true
    },
    legend: {
      cursor: "pointer",
      itemclick: function (e: any) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
        } else {
          e.dataSeries.visible = true;
        }
        e.chart.render();
      }
    },
    data: [
      {
        type: "line",
        showInLegend: true,
        name: "Incomes (services)",
        xValueFormatString: "MMM",
        dataPoints: []
      },
      {
        type: "line",
        showInLegend: true,
        name: "Incomes (details)",
        xValueFormatString: "MMM",
        dataPoints: []
      },
      {
        type: "line",
        showInLegend: true,
        name: "Incomes (summarily)",
        xValueFormatString: "MMM",
        dataPoints: []
      }
    ]
  }

  downloadData() {
    const year = this.year - this.yearsCount + this.pageIndex + 1
    const fromDate = new Date(year, 0, 2);
    const toDate = new Date(year, 11, 32);
    this.analyticsService.getData(fromDate, toDate).subscribe(result => {
      const json = JSON.stringify(result);
      const blob = new Blob([json], { type: 'application/json' });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'Data.json';
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    });
  }
}
