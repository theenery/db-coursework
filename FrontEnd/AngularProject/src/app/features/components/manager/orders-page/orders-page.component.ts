import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerOrder } from '../../../../core/models/manager-order';
import { OrderStatus } from '../../../../core/models/order-status';
import { OrderStatusService } from '../../../../core/services/main/order-status.service';
import { OrderService } from '../../../../core/services/manager/order.service';
import { HeaderComponent } from '../../../../shared/components/header/header.component';
import { PaginatorComponent, PaginatorEvent } from '../../../../shared/components/paginator/paginator.component';
import { DatePipe } from '../../../../shared/pipes/date.pipe';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-manager-orders-page',
  standalone: true,
  imports: [
    CommonModule,
    DatePipe,
    HeaderComponent,
    MatFormFieldModule,
    MatSelectModule,
    PaginatorComponent
  ],
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnInit {
  isDescending = false;
  orders: ManagerOrder[] = [];
  orderStatuses: OrderStatus[] = [];
  orderStatusId = 0;
  orderCount = 0;
  pageIndex = 0;
  pageSize = 8;
  sortingOption = 'date';
  sortingOptions = ['date', 'status'];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService,
    private orderStatusService: OrderStatusService
  ) {}

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.isDescending = params['isDescending'] ?? false;
        this.pageIndex = params['pageIndex'] ?? 0;
        this.pageSize = params['pageSize'] ?? 8;
        this.sortingOption = params['sortingOption'] ?? 'date';
        this.orderStatusId = Number.parseInt(params['orderStatusId'] ?? '0');

        this.orderService.getCount(this.orderStatusId).subscribe(result => {
          this.orderCount = result;
          this.fetchOrders();
        });

        this.orderStatusService.getAll().subscribe(result => {
          this.orderStatuses = result;
          this.orderStatuses.push({ orderStatusId: 0, orderStatusName: 'All' });
        })
      });
  }

  fetchOrders() {
    this.orderService
      .getPaged(
        this.pageSize,
        this.pageIndex,
        this.sortingOption,
        this.isDescending,
        this.orderStatusId)
      .subscribe(result => {
        this.orders = result;
      });
  }

  setFilter() {
    this.pageIndex = 0;
    this.update({isDescending:this.isDescending, sortingOption: this.sortingOption, pageIndex: this.pageIndex, pageSize: this.pageSize});
  }

  goToOrder(id: number) {
    this.router.navigate(['manager-orders', id]);
  }

  update(e: PaginatorEvent) {
    this.router.navigate(
      ['manager-orders'],
      {
        queryParams: { ...e, orderStatusId: this.orderStatusId }
      });
  }
}
