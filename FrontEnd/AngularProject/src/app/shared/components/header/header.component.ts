import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { AccountService } from '../../../core/services/main/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [AccountService]
})
export class HeaderComponent {
  @Input() header = '';
  isEmployee() { return localStorage.getItem('role') == 'Employee'; }
  isManager() { return localStorage.getItem('role') == 'Manager'; }
  isSignIn = this.getSignedIn();
  username = this.getUsername();

  constructor(
    private accountService: AccountService,
    private router: Router) { }

  getSignedIn() {
    return this.accountService.isSignedIn();
  }

  getUsername() {
    return this.accountService.isSignedIn()
      ? `${localStorage.getItem('firstname')} ${localStorage.getItem('lastname')}`
      : 'Account';
  }

  goAnalytics() {
    this.router.navigate(['manager-analytics']);
  }

  goDetails() {
    const role = localStorage.getItem('role');
    if (role == "Manager") {
      this.router.navigate(['manager-details']);
    }
    else if (role == "Employee") {
      this.router.navigate(['employee-details']);
    }
  }

  goOrderDetails() {
    this.router.navigate(['manager-order-details']);
  }

  goHome() {
    this.router.navigate(['']);
  }

  goOrders() {
    const role = localStorage.getItem('role');
    if (role == "Manager") {
      this.router.navigate(['/manager-orders']);
    }
    else if (role == "Employee") {
      this.router.navigate(['/employee-orders']);
    }
    else {
      this.router.navigate(['/client-orders']);
    }
  }

  signIn() {
    this.router.navigate(['/sign-in']);
  }

  signOut() {
    this.accountService.signOut();
    location.reload(); // magic trick to update page with route ''
    this.router.navigate(['']);
  }
}
