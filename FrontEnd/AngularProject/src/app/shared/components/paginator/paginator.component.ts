import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatSelectChange, MatSelectModule } from '@angular/material/select';

export interface PaginatorEvent {
  isDescending: boolean,
  sortingOption: string,
  pageIndex: number,
  pageSize: number,
}

@Component({
  selector: 'app-paginator',
  standalone: true,
  imports: [
    CommonModule,
    MatSelectModule,
    MatPaginatorModule,
    MatCheckboxModule,
  ],
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent {
  @Output() newPage = new EventEmitter<PaginatorEvent>();
  @Input() count = 0;
  @Input() isDescending = false;
  @Input() sortingOption = '';
  @Input() sortingOptions: string[] = [];
  @Input() pageIndex = 0;
  @Input() pageSize = 8;
  @Input() pageSizeOptions = [8, 20, 40];

  handlePageEvent(e: PageEvent) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.emit();
  }

  emit() {
    this.newPage.emit({
      isDescending: this.isDescending,
      sortingOption: this.sortingOption,
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
    });
  }

  setDescending(value: boolean) {
    this.isDescending = value;
    this.emit();
  }

  setSorting(e: MatSelectChange) {
    this.sortingOption = e.value;
    this.emit();
  }
}
