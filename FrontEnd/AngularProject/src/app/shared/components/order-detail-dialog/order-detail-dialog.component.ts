import { CommonModule } from "@angular/common";
import { Component, Inject } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatChipsModule } from "@angular/material/chips";
import { MatOptionModule } from "@angular/material/core";
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { OrderDetail } from "../../../core/models/order-detail";
import { Detail } from "../../../core/models/detail";
import { DetailService } from "../../../core/services/employee/detail.service";
import { MatAutocompleteModule } from '@angular/material/autocomplete';


export interface OrderDetailDialogData {
  detail: OrderDetail,
  action: string,
  deviceType: number
}

@Component({
  selector: 'app-order-detail-dialog',
  templateUrl: './order-detail-dialog.component.html',
  styleUrls: ['./order-detail-dialog.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatAutocompleteModule,
  ],
})
export class OrderDetailDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<OrderDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OrderDetailDialogData,
    private detailService: DetailService
  ) { }

  details: Detail[] = [];

  onNoClick(): void {
    this.dialogRef.close();
  }

  selectDetail(detail: Detail) {
    this.data.detail.detailId = detail.detailId;
    this.data.detail.detailName = detail.detailName;
    this.data.detail.detailPrice = detail.detailPrice;
    this.data.detail.detailCount = 1;
    this.data.detail.orderDetailStatus = { orderDetailStatusId: 1, orderDetailStatusName: 'Under consideration' };
  }

  update() {
    this.detailService
      .getPaged(this.data.deviceType, this.data.detail.detailName, 20, 0, 'name', false)
      .subscribe(result => {
        this.details = result;
      });
  }
}
