import { CommonModule } from "@angular/common";
import { Component, Inject } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatChipsModule } from "@angular/material/chips";
import { MatOptionModule } from "@angular/material/core";
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { DetailWithDeviceTypes } from "../../../core/models/create-detail";
import { DeviceType } from "../../../core/models/device-type";


export interface DetailDialogData {
  detail: DetailWithDeviceTypes;
  action: string;
  deviceTypes: DeviceType[]
}

@Component({
  selector: 'app-detail-dialog',
  templateUrl: './detail-dialog.component.html',
  styleUrls: ['./detail-dialog.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    MatChipsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
  ],
})
export class DetailDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DetailDialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addDeviceType(dt: DeviceType) {
    if (!this.data.detail.deviceTypes.map(dt => dt.deviceTypeId).includes(dt.deviceTypeId)) {
      this.data.detail.deviceTypes.push(dt);
    }
  }

  removeDeviceType(dt: DeviceType) {
    const index = this.data.detail.deviceTypes.indexOf(dt);
    if (index !== -1) {
      this.data.detail.deviceTypes.splice(index, 1);
    }
  }
}
