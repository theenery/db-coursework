import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date',
  standalone: true
})
export class DatePipe implements PipeTransform {

  transform(value: Date, ...args: unknown[]): unknown {
    return new Date(value).toLocaleDateString();
  }
}
